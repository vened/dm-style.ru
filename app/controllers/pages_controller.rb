class PagesController < ApplicationController

  def index
    @page = Page.includes(:galleries).find_by_path('root')
    # @products = Product.includes(:photos).where(:new => true)
    @brands = Brand.includes(:photos).all.order('lft ASC')
    @villa = @brands.includes(:photos, :products => [:photos, :categories]).find_by_name('Villa ceramica')

    @bathroom_tile = Category.includes(:products).find_by_path('bathroom-tile')
    # @villa = Brand.find_by_name('Villa ceramica')
  end

  def show
    @page = Page.includes(:galleries).find_by_path(params[:id])
  end

  def carousel
    @carousel = Gallery.includes(:photos).find_by_slider(true)
    @photos = @carousel.photos
    render json: @photos
  end

  def catalog
    # @categories = Category.all.order('lft ASC')
    @products = Product.joins(:categories, :photos).page(params[:page])
  end

  def category
    @brands = Brand.all.order('lft ASC')
    @category = Category.includes(:products).find_by_path(params[:id])
    @properties = Property.all
  end

  def brands
    @brands = Brand.all.order('lft ASC')
  end

  def brand
    @categories = Category.all.order('lft ASC')
    @brand = Brand.includes(:products).find_by_path(params[:id])
    @properties = Property.all
  end

  def product
    @product = Product.includes(:categories,:brands, :photos).find(params[:product_id])
    @category = @product.categories.first
    @brand_photo = @product.brands.first.photos.first.photo.brand
  end

  private

  def product_params
    params.require(:product).permit(:product_id, :category_id, :brand_id, :price, :title, :body, :gallery_id)
  end

end
