-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- Dump data of "brands" -----------------------------------
INSERT INTO `brands`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`) VALUES ( '2', 'Almera ceramica', NULL, NULL, '9', '10', '0', 'almera-ceramica' );
INSERT INTO `brands`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`) VALUES ( '3', 'Roca', NULL, NULL, '3', '4', '0', 'roca' );
INSERT INTO `brands`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`) VALUES ( '4', 'APE', NULL, NULL, '7', '8', '0', 'ape' );
INSERT INTO `brands`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`) VALUES ( '5', 'FANAL', NULL, NULL, '5', '6', '0', 'fanal' );
INSERT INTO `brands`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`) VALUES ( '6', 'Realonda', NULL, NULL, '11', '12', '0', 'realonda' );
INSERT INTO `brands`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`) VALUES ( '7', 'Villa ceramica', NULL, NULL, '1', '2', '0', 'villa-ceramica' );
-- ---------------------------------------------------------


-- Dump data of "brands_photos" ----------------------------
INSERT INTO `brands_photos`(`brand_id`,`photo_id`) VALUES ( '2', '17' );
INSERT INTO `brands_photos`(`brand_id`,`photo_id`) VALUES ( '3', '18' );
INSERT INTO `brands_photos`(`brand_id`,`photo_id`) VALUES ( '4', '19' );
INSERT INTO `brands_photos`(`brand_id`,`photo_id`) VALUES ( '5', '20' );
INSERT INTO `brands_photos`(`brand_id`,`photo_id`) VALUES ( '6', '22' );
INSERT INTO `brands_photos`(`brand_id`,`photo_id`) VALUES ( '7', '25' );
-- ---------------------------------------------------------


-- Dump data of "brands_products" --------------------------
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '2', '29' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '3', '23' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '3', '24' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '3', '25' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '3', '26' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '5', '27' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '5', '28' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '7' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '8' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '9' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '10' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '11' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '12' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '13' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '14' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '15' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '16' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '17' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '18' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '19' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '20' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '21' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '22' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '30' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '31' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '32' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '33' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '34' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '35' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '36' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '37' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '38' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '39' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '40' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '41' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '42' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '43' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '44' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '45' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '46' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '47' );
INSERT INTO `brands_products`(`brand_id`,`product_id`) VALUES ( '7', '48' );
-- ---------------------------------------------------------


-- Dump data of "categories" -------------------------------
INSERT INTO `categories`(`id`,`name`,`sortable`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`body`) VALUES ( '1', 'Плитка для ванной', NULL, NULL, '1', '2', '0', 'bathroom-tile', '<p>В нашем ассортименте присутствует огромный ассортимент коллекций керамической плитки для ванной комнаты - &nbsp;помещения, где исторически используют керамическую плитку как основной материал для облицовки стен и полов. Размеры для настенной плитки ограничиваются только возможностями производства и техническими особенностями. С годами оказался полностью разрушен стереотип о том, что крупноформатная плитка подходит только для больших объёмных помещений. Как показала практика - в маленьких и стандартных санузлах, крупноформатная плитка просто преображает и&nbsp;зрительно увеличивает&nbsp;помещение. &nbsp;А декоративные элементы различных стилей, форм и дизайнов позволяют осуществить любую, самую смелую задумку дизайнера.&nbsp;</p>
' );
INSERT INTO `categories`(`id`,`name`,`sortable`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`body`) VALUES ( '2', 'Плитка для кухни', NULL, NULL, '3', '4', '0', 'tiles-for-kitchen', NULL );
INSERT INTO `categories`(`id`,`name`,`sortable`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`body`) VALUES ( '3', 'Плитка для пола', NULL, NULL, '5', '6', '0', 'floors-tiles', '<p>Керамическая плитка для пола в нашем ассортименте представлена практически в каждом бренде. У нас есть глазурованный и полированный керамогранит с бордюрами и угловыми вставками. Есть и красивые панно- как изготовленные с помощью гидроабразивной резки, так и с использованием 3-хкратного или 4-хкратного обжига. Есть и напольная плитка сделанная из белой или красной глины. Всевозможные размеры: 300*300, 350*350, 450*450, 500*500 и 600*600. Смотрите, выбирайте, оценивате...</p>
' );
INSERT INTO `categories`(`id`,`name`,`sortable`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`body`) VALUES ( '4', 'Мозаика', NULL, NULL, '7', '8', '0', 'mosaic', '<p>Многие из тех, кто занимается ремонтом своего жилья, задаются вопросом: &quot;Чем отделать унылые стены ванной комнаты или бассейна, полы в коридоре,&nbsp; фартук на кухне, если обычная керамическая плитка уже надоела?&quot;. &nbsp;&quot;Например, мозаикой, - ответим мы. - Именно она способна соответствовать почти всем стилям (от минимализма до барокко) и запросам (подойдет и для простого декора, и для художественного полотна)&quot;.</p>

<p>По своим эксплуатационным качествам мозаика не знает себе равных. Это невероятно прочный материал, выдерживающий сильные перепады влажности и температур. В частности, мозаику широко применяют для различных поверхностей в турецких банях (хамамах), а также для отделки чаш бассейнов. Это обусловлено тем, что у данного материала практически нулевой коэффициент влагопоглощения. Также, мозаику керамическую и стеклянную можно использовать&nbsp; для отделки каминов и печей, из-за её высокой жаропрочности. Наиболее популярной у покупателей сегодня является мозаика для ванной, входящая в состав множества коллекций керамической плитки в виде элементов коллекции.&nbsp;</p>

<p>На нашем рынке присутствует мозаика из стекла, смальты, камня, керамогранита, керамической плитки и даже металла.&nbsp;</p>
' );
INSERT INTO `categories`(`id`,`name`,`sortable`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`body`) VALUES ( '6', 'Панно из керамогранита', NULL, NULL, '9', '10', '0', 'keramogranit', NULL );
INSERT INTO `categories`(`id`,`name`,`sortable`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`body`) VALUES ( '7', 'Декоративные элементы', NULL, NULL, '11', '12', '0', 'decorative-items', NULL );
-- ---------------------------------------------------------


-- Dump data of "categories_photos" ------------------------
-- ---------------------------------------------------------


-- Dump data of "categories_products" ----------------------
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '7' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '8' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '9' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '10' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '11' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '12' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '13' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '14' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '15' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '16' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '17' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '18' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '19' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '20' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '21' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '22' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '23' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '24' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '25' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '26' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '27' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '28' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '29' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '1', '48' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '40' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '41' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '42' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '43' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '44' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '45' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '46' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '3', '47' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '4', '35' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '4', '36' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '4', '37' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '4', '38' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '4', '39' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '6', '30' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '6', '31' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '6', '32' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '7', '33' );
INSERT INTO `categories_products`(`category_id`,`product_id`) VALUES ( '7', '34' );
-- ---------------------------------------------------------


-- Dump data of "feedbacks" --------------------------------
-- ---------------------------------------------------------


-- Dump data of "galleries" --------------------------------
INSERT INTO `galleries`(`id`,`name`,`system_name`,`body`,`slider`) VALUES ( '1', 'Слайдер на главной', NULL, NULL, '1' );
-- ---------------------------------------------------------


-- Dump data of "galleries_pages" --------------------------
-- ---------------------------------------------------------


-- Dump data of "galleries_photos" -------------------------
INSERT INTO `galleries_photos`(`gallery_id`,`photo_id`) VALUES ( '1', '23' );
-- ---------------------------------------------------------


-- Dump data of "managers" ---------------------------------
INSERT INTO `managers`(`id`,`name`,`email`,`password_digest`,`remember_token`,`created_at`,`updated_at`) VALUES ( '1', 'admin', 'maxstbn@gmail.com', '$2a$10$EHzKLvnPrVoEt4X417yiT.oD4sDXhc/nZxTZWsiXmQhuAs6PvBbhO', '63713e7afc94301f6973ee85b450bcb566c2b0bf', '2014-10-20 07:56:13', '2014-10-27 12:27:02' );
INSERT INTO `managers`(`id`,`name`,`email`,`password_digest`,`remember_token`,`created_at`,`updated_at`) VALUES ( '2', 'admin', 'admin@dm-style.ru', '$2a$10$oTDG3bqfLPgh8zBHjzybSuAoHS9mbTSIa1iOUsejkvMvLk2/6NSJq', 'c567db7947cf5f450ea88582bdff73d3348ded1f', '2014-10-27 12:28:10', '2014-10-27 12:28:10' );
-- ---------------------------------------------------------


-- Dump data of "news" -------------------------------------
INSERT INTO `news`(`id`,`path`,`name`,`body`,`public`,`created_at`,`updated_at`) VALUES ( '1', NULL, 'Осенняя распродажа', '<p>В рамках акции&nbsp;&quot; <strong>Успей забрать свое</strong> &quot;, мы дарим Вам скидки на коллекцию Nocturne.</p>
', NULL, '2013-09-21 00:00:00', '2014-10-21 07:15:57' );
INSERT INTO `news`(`id`,`path`,`name`,`body`,`public`,`created_at`,`updated_at`) VALUES ( '2', NULL, 'Коллекция Expirience', '<p>Кому-то интерьер ванной, облицованной плиткой, имитирующей золото, может показаться слишком вычурным. Однако для того, чтобы придать ванной лоска и блеска вовсе не обязательно превращать ее в золотой ларец или во дворец мифического царя Мидаса и выкладывать ее драгоценной керамикой от пола до потолка.</p>
', NULL, '2013-06-16 00:00:00', '2014-10-21 07:16:55' );
INSERT INTO `news`(`id`,`path`,`name`,`body`,`public`,`created_at`,`updated_at`) VALUES ( '3', NULL, 'Коллекция Miracolo от Villa Ceramica', '<p>Представляем вам очередную монохромную новинку от Villa Ceramica - коллекцию Miracolo.&nbsp;</p>

<p>Коллекция представлена в популярной чёрно-белой гамме, с декоративвными панелями в стиле &quot;буазере&quot; а также фоновой плиткой с монохромным орнаментом. Также в коллекции привутствуют не только бордюры -&quot;бусинки&quot;, но и металлизированные бордюры-карандаши со стальной окантовкой.</p>

<p><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/001/large/miracolo.jpg"><img alt="" data-rich-file-id="1" src="/system/rich/rich_files/rich_files/000/000/001/large/miracolo.jpg" /></a></p>

<p><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/002/large/hmlc60362a.jpg"><img alt="" data-rich-file-id="2" height="572" src="/system/rich/rich_files/rich_files/000/000/002/large/hmlc60362a.jpg" width="281" /></a><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/003/large/hmlb60361a-300x600-.jpg"><img alt="" data-rich-file-id="3" height="572" src="/system/rich/rich_files/rich_files/000/000/003/large/hmlb60361a-300x600-.jpg" width="286" /></a></p>

<p>Также в коллекции присутствуют черные и белые плинтусы</p>
', NULL, '2013-05-25 00:00:00', '2014-10-21 07:20:56' );
INSERT INTO `news`(`id`,`path`,`name`,`body`,`public`,`created_at`,`updated_at`) VALUES ( '4', NULL, 'История камня от Villa Ceramica', '<p>К началу строительного сезона представляем коллекцию&nbsp;Storie Di Pietra &nbsp;&nbsp;из глазурованного керамогранита&nbsp;от Villa Ceramica.&nbsp;</p>

<p>Уникальный дизайн коллекции наверняка придётся по вкусу владельцам загородных домов, коттеджей. Также плитка их коллекции Storie Di Pietra прекрасно подойдёт для облицовки полов и стен вне помещения - веранды, беседки, террасы и зимние сады.&nbsp;</p>

<p><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/004/large/storie-20di-20pietra.jpg"><img alt="" data-rich-file-id="4" src="/system/rich/rich_files/rich_files/000/000/004/large/storie-20di-20pietra.jpg" /></a></p>
', NULL, '2013-05-21 00:00:00', '2014-10-21 07:22:18' );
-- ---------------------------------------------------------


-- Dump data of "pages" ------------------------------------
INSERT INTO `pages`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`feedback`,`menu`) VALUES ( '1', 'Главная', '<p>Напольная плитка является стильным решением для желающих облагородить свое жилье с помощью изысканных отделочных материалов. Основными достоинствами керамической 
          плитки являются прочность, долговечность и эстетичность. Дизайнерские находки воплощены в сотнях коллекций напольной плитки, осталось лишь найти ту, которая подойдет Вам.</p>
          <p>Напольная плитка является стильным решением для желающих облагородить свое жилье с помощью изысканных отделочных материалов. Основными достоинствами керамической 
          плитки являются прочность, долговечность и эстетичность. Дизайнерские находки воплощены в сотнях коллекций напольной плитки, осталось лишь найти ту, которая подойдет Вам.</p>', NULL, '1', '2', '0', 'root', NULL, '1' );
INSERT INTO `pages`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`feedback`,`menu`) VALUES ( '2', 'Контакты', '<h2>Адрес офиса:</h2>

<p>Россия, г. Москва Нахимовский проспект, 24. ТВК &laquo;Экспострой&raquo;, 5 павильон, офис 18</p>

<p><strong>Телефон:</strong> +7 (495) 668-11-76 (многоканальный), &nbsp;8-906-730-40-52</p>

<p><strong>E-mail:</strong>&nbsp; <a href="mailto:info@villaceramica.ru">info@villaceramica.ru</a></p>

<p><strong>Время работы:</strong>&nbsp; понедельник-пятница с 10-00 до 19-00</p>

<p>Суббота и воскресенье &ndash;выходные</p>

<h2>Коммерческий отдел:</h2>

<p>Менеджер по развитию клиентов <strong>Бодрых Денис</strong></p>

<p><strong>E-mail: </strong><a href="mailto:servismeneger@santemax.ru">servismeneger@santemax.ru</a></p>

<p>Менеджер по развитию клиентов &nbsp;<strong>Курникова Елена</strong></p>

<p><strong>E-mai: </strong><a href="mailto:e.kurnikova@santemax.ru">e.kurnikova@santemax.ru</a></p>

<h2>Схема проезда в офис:</h2>
<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=7D2fTTTiXtm0wfTekzqz-ZD0r9yLKawm&width=720&height=550"></script>

<h2>Адрес склада:</h2>

<p>МО, Чеховский район, сельское поселение Баранцевское, промзона Новоселки</p>

<p><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/008/large/sclad.jpg"><img alt="" data-rich-file-id="8" src="/system/rich/rich_files/rich_files/000/000/008/large/sclad.jpg" /></a></p>

<p>По Симферопольскому шоссе &ndash; 70-й километр до указателя &laquo;<strong><span style="text-decoration: underline;">Поворот на г. Чехов/ст.Крюково</span></strong>&raquo;.</p>

<p><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/009/large/sclad1.jpg"><img alt="" data-rich-file-id="9" src="/system/rich/rich_files/rich_files/000/000/009/large/sclad1.jpg" /></a></p>

<p>После съезда, приблизительно через 600 метров &ndash; первый поворот налево после указателя <strong>&laquo;д.Новосёлки&raquo;. </strong>Двигаться по главной дороге, затем поворот направо. Напротив здания &laquo;Логистик-Центра&raquo; поворот направо в сторону складского комплекса <strong>DM-</strong><strong>Style</strong>. Въезд через синие ворота. Оформление документов в административном здании слева на 2-м этаже.</p>

<p><strong>Телефон склада: </strong><strong>8-906-730-42-05 (<strong>Марина Викторовна)</strong></strong></p>

<p><strong>Отпуск товара осуществляется только при наличии доверенности!!!!!!</strong></p>

<p>Время работы складского комплекса на отгрузку: понедельник &ndash;пятница с 9-00 до 18-00. Суббота и воскресенье &ndash; выходные.</p>

<h2>Напишите нам сообщение:</h2>
', NULL, '11', '12', '0', 'contacts', '1', '1' );
INSERT INTO `pages`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`feedback`,`menu`) VALUES ( '5', 'О компании', '<p><strong>DM-Style </strong>&ndash; &nbsp;молодая и активно развивающаяся компания в сфере &nbsp;оптовых продаж керамической плитки, керамогранита и мозаики. Мы стремимся &nbsp;обеспечить нашим партнёрам &ndash; специализированным салонам-магазинам - &nbsp;удобные и комфортные условия работы со всеми видами напольных и настенных керамических покрытий. В нашем ассортименте имеются также: плитка &quot;под паркет&quot;, глазурованный и полированный керамогранит, напольная плитка с тактильным противоскользящим эффектом, а также линейка универсальных декоративных элементов.</p>

<p>Большой выбор расцветок, размеров и стилей позволяет выбрать подходящую коллекцию производителей таких стран как Турция, Испания и Китай для отделки любого объекта.</p>

<p>Нашими специалистами достаточно долго изучался рынок керамических покрытий и итогом их анализа стал ассортимент коллекций, который мы предлагаем своим партнёрам.</p>

<p><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/001/original/miracolo.jpg"><img alt="" data-rich-file-id="1" src="/system/rich/rich_files/rich_files/000/000/001/thumb/miracolo.jpg" /></a><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/005/original/marrakesh.jpg"><img alt="" data-rich-file-id="5" src="/system/rich/rich_files/rich_files/000/000/005/thumb/marrakesh.jpg" /></a><a class="insert-img" href="/system/rich/rich_files/rich_files/000/000/007/original/pastel.jpg"><img alt="" data-rich-file-id="7" src="/system/rich/rich_files/rich_files/000/000/007/thumb/pastel.jpg" /></a></p>

<p>Продукция производства Испании &ndash; это традиционно качественная плитка, выполненная мастерами своего дела. Действующая в стране Ассоциация производителей керамики Ascer, объединяет в себе более 160 компаний и это не случайно, &nbsp;т.к. именно производство керамической продукции является одним из ключевых видов отраслей в экономике.</p>

<p>Компания <strong>DM-Style</strong> является официальным представителем на территории России и &nbsp;стран СНГ бренда керамической плитки и мозаики &laquo;<strong>VILLA CERAMICA</strong>&raquo;.&nbsp;</p>

<p>Изготавливается продукция <strong>&laquo;VILLA CERAMICA&raquo;</strong> на территории Китая, но дизайн и качество находятся под контролем итальянских специалистов. Истоки производства керамической продукции уходят в далекие времена, когда Китай являлся основоположником данного направления. На сегодняшний день КНР &nbsp;производит более трети всего мирового объёма производства керамической плитки, при этом менее 10% от этого объёма &ndash; способно отвечать жёстким европейским стандартам. Под брендом <strong>&laquo;VILLA CERAMICA&raquo;</strong> производится продукция на современных фабриках, &nbsp;выпускающих плитку и керамогранит высокого, европейского качества.&nbsp;</p>

<p>Отличительными особенностями продукции <strong>&laquo;VILLA CERAMICA&raquo;</strong> являются:</p>

<ul>
	<li>Вся облицовочная плитка производится на белой глине и в 99% - ректифицирована. Она имеет обрезанные под углом 90&deg; кромки - это делается для достижения точных размеров. Подобная плитка позволяет делать бесшовную укладку (напомним, что таковой считается укладка с шириной шва от 0 до 2 мм) &ndash; что говорит о высочайшем качестве продукции. Редкие фабрики Италии, Португалии и Испании могут себе позволить подобное качество.</li>
	<li>Большинство декоративных вставок к облицовочной плитке продаются не поштучно, как у испанских и итальянских производителей, а кратно квадратным метрам &ndash; что заметно упрощает реализацию коллекций с их использованием, так как многих потребителей отталкивает именно высокая стоимость штучных декоративных элементов.</li>
	<li>Качественная упаковка штучных декоративных элементов. Вы можете быть уверены, что к Вам придёт заказ для Вашего клиента целым и невредимым, так как все бордюры, карандаши, бусинки и прочие элементы упаковываются в боксы из пенопласта. Что по сравнению с картонными коробками на порядок удобнее для хранения и транспортировки непосредственно до конечного покупателя.</li>
	<li>Современные и, самое главное коммерческие, дизайны коллекций &ndash; результат творческой работы итальянских дизайнеров и технологов с китайскими производителями.</li>
	<li>Широкая линейка размеров облицовочной плитки: 300*450; 240*660; 270*730; 300*600; 350*750; 400*800.</li>
	<li>Большинство коллекций гармонично &laquo;обыграны&raquo; эксклюзивной мозаикой. Но при этом они ориентированы не только на санузлы, но и на гостиные, прихожие, кухни и студии.</li>
	<li>Качественный &laquo;европейский&raquo; мерчендайзинг. Современная презентация продукции позволит нашим партнёрам эффективно использовать свои выставочные площади за счёт уникальных дизайнов технических панелей, реалистичных интерьеров и современного оформления.</li>
</ul>

<p><strong>Ну и самая главная &laquo;фишка&raquo; - это конечно ЦЕНА!!! Стоимость коллекций &laquo;VILLA CERAMICA&raquo; несколько ниже продукции традиционных испанских производителей, но при этом,&nbsp;DM-Style предлагает вам работать не с красной глиной, а с ректификатом на белой глине, &nbsp;и не переплачивать за имена и бренды.</strong></p>

<p>Приглашаем к сотрудничеству как московские, так и региональные торговые компании. Оптовым покупателям мы готовы предложить оптимальные, конкурентоспособные &nbsp;цены и скидки, образцы, рекламные материалы и консультационную поддержку, удобные условия сотрудничества, доставку до транспортной компании для региональных клиентов.</p>

<p><strong>По коллекциям из оптового ассортимента постоянно поддерживаются обширные складские остатки!!!</strong></p>
', NULL, '3', '4', '0', 'company', NULL, '1' );
INSERT INTO `pages`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`feedback`,`menu`) VALUES ( '6', 'Для дилеров', '<p><strong>DM-Style</strong> не просто выводит на российский рынок новый бренд <strong>&laquo;VILLA CERAMICA&raquo;</strong>, но и предлагает своим партнёрам качественный, современный и актуальный сервис:</p>

<h2>СКЛАДСКАЯ и ТРАНСПОРТНАЯ ЛОГИСТИКА</h2>

<ul>
	<li>Комплексная и профессиональная отгрузка с центрального склада в Чеховском районе Московской области.</li>
	<li>Работа центрального склада с 9.00 до 19.00 с понедельника по пятницу.</li>
	<li>Возможность оперативной отгрузки &ndash; в день поступления заказа.</li>
	<li>Доставка до транспортных компаний и до торговых точек в Москве и Московской области.</li>
</ul>

<h2>АССОРТИМЕНТ</h2>

<ul>
	<li>Мы оперативно предоставим вам выставочное оборудование и технические панели&nbsp;&ndash; срок производства технических панелей от 3-х до 5 рабочих дней. Каталог технических панелей и экспозиторов размещён в &quot;Разделе для Дилеров&quot;</li>
	<li>Большие складские запасы и гарантированная комплектность коллекций&nbsp;&ndash; Мы стараемся обеспечить максимальную комплектацию всех имеющихся у нас коллекций.</li>
	<li>Эксклюзивный ассортимент&nbsp;&ndash; мы позволим вам выгодно отличаться от остальных продавцов плитки. А ширина и многообразие ассортимента позволят вам предложить своим покупателям керамическую плитку на любой вкус.</li>
</ul>

<h2>КЛИЕНТСКАЯ ПОДДЕРЖКА</h2>

<ul>
	<li><strong>Гибкая ценовая политика</strong>&nbsp;&ndash; мы всячески поддерживаем своих постоянных партнёров и создаём для них максимально комфортные условия работы с нами. Оптовый клиент (не торговая организация), приобретающий плитку для собственных нужд никогда не получит дилерскую цену, его цена всегда будет выше.</li>
	<li><strong>Отгрузка от любого объема (от штуки) с сохранением дилерской цены</strong>&nbsp;&ndash; мы Вас ни в чём не ограничиваем.</li>
	<li><strong>Работа с квалифицированными и высокопрофессиональными менеджерами</strong>.&nbsp;Опыт работы наших менеджеров на рынке керамики не менее 3-х лет.&nbsp;</li>
	<li><strong>Индивидуальное обслуживание каждого клиента</strong>.&nbsp; Раздел для постоянных дилеров на сайте где всегда в актуальном состоянии необходимая для работы информация:</li>
</ul>

<p>- шаблоны договоров<br />
- дилерские прайс-листы и рекомендованные розничные цены<br />
- каталоги мерчендайзинга по всему ассортименту<br />
- системы скидок и условий работы<br />
- эксклюзивные материалы рынка керамики<br />
- свежие новости и обзоры новинок</p>

<p>Став нашим дилером, &nbsp;Вы всегда будете на шаг впереди &nbsp;своих конкурентов, благодаря уникальному ассортименту, нашему опыту и стремлению к совершенствованию своих возможностей.</p>

<p>Для входа на дилерский раздел сайта, пожалуйста, укажите ваш ЛОГИН и ПАРОЛЬ,&nbsp; предоставленный вашей компании коммерческим отделом DM-Style.</p>

<h3>Как стать нашим дилером?</h3>

<ul>
	<li>Вы - торговая компания, &nbsp;у вас есть розничный торговый зал и своё выставочное оборудование?</li>
	<li>Вы хотите работать с новым и интересным брендом, которого нет у ваших конкурентов?</li>
	<li>Вы активно работаете с европейскими производителями плитки?</li>
	<li>Вы хотите серьёзно обновить ваш ассортимент в торговом зале?</li>
	<li>Вам нужен стабильный поставщик на долгосрочную перспективу?</li>
</ul>

<p>Если это про вас - тогда мы будем рады стать вашим поставщиком.&nbsp; Пишите на <a href="mailto:info@villa-ceramica.ru">info@villa-ceramica.ru</a>, звоните по телефону +7&nbsp;495&nbsp;668-11-76 в коммерческий отдел, или оставляйте ваш запрос через форму обратной связи.</p>

<p>После получения вашего запроса &ndash; с вами свяжется менеджер коммерческого отдела &nbsp;для уточнения деталей и заключения дилерского договора.&nbsp;После заключения дилерского договора вам будут предоставлены ЛОГИН и ПАРОЛЬ для входа в &laquo;РАЗДЕЛ ДЛЯ ДИЛЕРОВ&raquo;.&nbsp;</p>
', NULL, '5', '6', '0', 'dealers', NULL, '1' );
INSERT INTO `pages`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`feedback`,`menu`) VALUES ( '7', 'Наши новости', NULL, NULL, '7', '8', '0', 'news', NULL, '1' );
INSERT INTO `pages`(`id`,`name`,`body`,`parent_id`,`lft`,`rgt`,`depth`,`path`,`feedback`,`menu`) VALUES ( '8', 'Где купить', '<h2>Розничные торговые точки в г. Москве, где можно приобрести коллекции Villa&nbsp; Ceramica:</h2>

<p>1-й км. Киевского ш.&nbsp;БП &quot;Румянцево&quot;:&nbsp;Интерьерный салон АбадКерамика (пав.060В)</p>

<p>17-й км. МКАД (внешняя сторона). ТК &quot;М38&quot;.&nbsp;Салон &quot;Мир Керамической Плитки&quot;</p>

<p>ВК &quot;РосСтройЭкспо&quot;. Фрунзенская наб. 30: Салон &quot;Мир Камня&quot; (пав.27)</p>

<p>Горьковское ш. 1 . Салон &quot;Люкс Керам&quot;</p>

<p>Нахимовский пр-т, 28. Салон &laquo;Плиткамакс&raquo;</p>

<p>Намёткина ул. 3. Салон &quot;Мама-Керамика&quot;</p>

<p>Строительный Двор &quot;Петровский&quot; Новорижское ш. (9 км. от МКАД):&nbsp;Салон &quot;Glass&amp;Stone&quot; (пав. Г-1)&nbsp;</p>

<p>Строительный рынок &laquo;Славянский бульвар&raquo; 41 км. МКАД:&nbsp;ИП Маринин (пав. НПВ-80)</p>

<p>Строительный рынок &laquo;Славянский бульвар&raquo; 41 км. МКАД: ИП Соколова Е.А. (пав. А4/4)</p>

<p>Строительный рынок &laquo;Славянский бульвар&raquo; 41 км. МКАД: ИП Гурский Петр Борисович (Б16/3)</p>

<p>Строительный рынок &laquo;Каширский Двор -3&raquo;: ИП Соколова Е.А (пав. С3-3)</p>

<p>ТЦ &quot;Конструктор&quot;, МКАД 25 км. ИП Соколова Е.А. (пав. Д 1-18,19)</p>

<p><br />
ТВК &laquo;Экспострой&raquo;, Нахимовский пр-т, 24: Салон &laquo;Santemax&raquo; (3-й пав., цокольный этаж)</p>

<p>ТСК &quot;Каширский Двор&quot;, Каширское ш. 19, корп.2. Салон &laquo;Santemax&raquo; (2-й этаж&nbsp; пав. 2-11; 2-40)</p>

<p>ТСК &quot;Каширский Двор&quot;, Каширское ш. 19, корп.2. Каолина (2-й этаж пав. 2-44)</p>

<p>Торговый Дом Мебели, Мневники, 10 кор.1. Салон &quot;ФЕШН - КЕРАМИКА&quot;</p>

<p>ТК Люблинское поле, Тихорецкий бульвар, 1а. Павильоны: Т-130, Т-144, Р-108</p>

<p>ТЦ &quot;Город&quot;, Рязанский пр-кт 2, корп.2. Салон &quot;Стильный Дом&quot; (2-й этаж пав.220)</p>

<p>Экспо Центр Дизайна MADEX, Рябиновая ул., 41 корпус 1. Антураж (ИП Конкин Б.В., 2-ой этаж)</p>

<p>ТЦ &quot;Элитстройматериалы&quot;, 51 км МКАД ( внешняя сторона). Салон- магазин &quot;ПАКО-Керамика&quot; ( пав.С-49, пав. С-50)</p>

<p>ТВЦ &quot;Никольский&quot;, Носовихинское шоссе, влад. № 4. ИП Филлипова А.А. (линия 1, пав. 1)</p>

<p>Центр Оптово-Розничной торговли &quot;БУХТА&quot;, Москва г, Дмитровское ш. д. 165 к.1. ООО &quot;АРБИСТ&quot;&nbsp;2 этаж, павильон &quot;2-Е-4&quot;.</p>

<p>Строительный рынок ТВЦ, 1,5 км. от МКАД по Можайскому ш., Магазин &quot;Модная Плитка&quot;</p>

<h2>Розничные торговые точки в Московской области, где можно приобрести коллекции Villa&nbsp; Ceramica:</h2>

<p>г. Воскресенск, ул. 2-ая Заводская 6. Торгово-Строительные ряды ИП Крестьянинов А.С.&nbsp;(пав. 3) магазин &quot;ОБЛКЕРАМИКА&quot;</p>

<p>г. Дмитров, Московская ул.,29. Салон &laquo;Мосплитка&raquo;</p>

<p>г. Дубна, пр-т Боголюбова,39. Салон &laquo;Мосплитка&raquo;</p>

<p>г. Мытищи, ул. Коммунистическая 25Г. Тракт-Терминал пав. Д 1</p>

<p>г. Реутов, Северный пр-д, д.1. ТК &quot;Мой Дом&quot; пав.18-20 Салон &quot;Империя Кафеля&quot;</p>

<h2>Розничные торговые точки в ЦФО, где можно приобрести коллекции Villa&nbsp; Ceramica:</h2>

<p>г. Владимир, ул. Горького, 27. Салон &quot;ЛедИ Керамика&quot;</p>

<p>г. Воронеж, ул. Пеше-Стрелецкая, 43 Магазин керамической плитки &quot;УЛЬТРА&quot;</p>

<p>г. Воронеж, ул. Бульвар Победы, 50 А Салон &quot;Элегант Стиль&quot;</p>

<p>г. Липецк,&nbsp; Товарный проезд, 1&raquo;А&raquo;. ТК &laquo;Новосёл&raquo;</p>

<p>г. Кострома,&nbsp; ул. Ивана Сусанина,30. &nbsp;Салон &quot;Мир Керамики&quot;</p>

<p>г. Рязань, ул. Яблочкова, 6 Салон керамической плитки &quot;Барон&quot;</p>

<p>г. Рязань, Косимовское шоссе, 27 Салон &quot;ВЕНГЕ&quot;</p>

<p>г. Рязань, Яблочкова, 6 стр. 1 Салон &quot;Стильный Дом&quot;</p>

<p>г. Смоленск,&nbsp; ул. Академика Петрова,14. &nbsp;Салон &quot;Мир Плитки&quot;</p>

<p>г. Смоленск,&nbsp; ул. Николаева, 42-а&nbsp;&nbsp;&nbsp; Салон &quot;LUIGI ROVATTI&quot;</p>

<p>г. Тула, ул. Мосина, 2 ТЦ &quot;СтройДвор&quot;</p>

<p>г. Ярославль, пр-т Фрунзе, 38. Салон-магазин &quot;Дом сантехники&quot;</p>

<p>г. Ярославль, ул. Угличская, 12 Б. &nbsp;Салон &quot;Ванные комнаты&quot;</p>

<h2>Розничные торговые точки в Приволжском федеральном округе, где можно приобрести коллекции Villa Ceramica:</h2>

<p>Республика Башкортостан, г. Уфа, ул. Индустриальная, 44/1 отдел 1В-8. Декор-керамика-Уфа</p>

<p>Республика Башкортостан, г. Уфа, ул. Менделеева, 137 отдел В1. Декор-керамика-Уфа</p>

<p>Республика Башкортостан, г. Уфа, ул. Большая Гражданская, 47. Салон &quot;Ufavanna.ru&quot;</p>

<h2>Розничные торговые точки в Республики Чувашия, где можно приобрести коллекции Villa Ceramica:</h2>

<p>г. Чебоксары, пр. Максима Горького 38/2. Салон &quot;Keramika House&quot;</p>

<p>г. Чебоксары, ул. Калинина, 105а, ТРЦ &quot;Мега молл&quot; 1 этаж. Салон &quot;Leonardo&quot;</p>

<h2>Розничные торговые точки Северо-Кавказкого ФО, где можно приобрести коллекции Villa Ceramica:<br />
Республика Северная Осетия - Алания</h2>

<p>г. Владикавказ, &nbsp;ул. Тельмана, дом № 18а. Строй маркет &quot;Марио&quot;</p>

<p>г. Владикавказ, ул. Гастелло д. 73, &quot;Галерея Керамики&quot;</p>
', NULL, '9', '10', '0', 'all-of-the-tiles', NULL, '1' );
-- ---------------------------------------------------------


-- Dump data of "photos" -----------------------------------
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '1', '0a27ec09508ecb335e7e7c269c0843ee.jpeg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '2', '53581.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '4', '0a27ec09508ecb335e7e7c269c0843ee.jpeg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '5', '53581.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '6', '0a27ec09508ecb335e7e7c269c0843ee.jpeg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '7', '53581.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '8', '0a27ec09508ecb335e7e7c269c0843ee.jpeg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '9', '53581.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '10', '0a27ec09508ecb335e7e7c269c0843ee.jpeg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '11', '53581.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '12', '0a27ec09508ecb335e7e7c269c0843ee.jpeg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '13', '53581.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '16', '________Villa_Ceramica.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '17', 'almera_logo.png', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '18', 'roca-logo.png', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '19', 'portada.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '20', 'fanal.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '22', 'realonda.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '23', '2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '24', 'Miracolo.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '25', '________Villa_Ceramica.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '26', '_______________.jpg', 'Perline Argento 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '27', '_____________.jpg', 'Perline Perla 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '28', '_______.jpg', 'Alzata Glossy Black 150*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '29', 'Oro_Branelli_Matita.jpg', 'Oro Branelli Matita 27*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '30', 'Argenta_Branelli_Matita.jpg', 'Argenta Branelli Matita 27*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '31', '_______________.jpg', 'Alzata Glossy White 150*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '32', '______.jpg', 'Boiserie Glossy Black rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '33', 'Damasco_Bianco_rett.jpg', 'Damasco Bianco rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '34', 'Damasco_Nero_rett.jpg', 'Damasco Nero rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '35', 'HMLB60361A_300X600_.jpg', 'Boiserie Glossy White rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '36', 'IMG024.jpg', 'Glossy White 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '37', 'IMG028.jpg', 'Glossy Black 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '38', '1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '39', '2.jpg', 'Fiore Marine Dec. 30x60', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '40', 'Pastel.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '41', 'Pastel_Armonia_Inserto.jpg', 'Pastel Armonia Inserto s/4 960*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '42', 'Pastel_Rosa_London.jpg', 'Pastel Rosa London 50*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '43', '________Crema.jpg', 'Perline Crema 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '44', '________Bronzo.jpg', 'Perline Bronzo 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '45', 'Pastel_Rosa_rett.jpg', 'Pastel Rosa rett. 240*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '46', 'Creativo_Crema_Nevicata.jpg', 'Creativo Crema Nevicata 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '47', 'Pastel_Rosa_Tacche.jpg', 'Pastel Rosa Tacche 330*330', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '48', '______2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '49', 'versailles1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '50', '________ORO.jpg', 'Perline Oro 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '51', 'MDS31802B1__90X300.jpg', 'Versailes Listello 90*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '52', 'Pietra_Calcare_Oro_.jpg', 'Pietra Calcare Oro 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '53', 'MDD30802_300x300.jpg', 'Versailes Vanilla 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '54', 'MDS31801_300x300.jpg', 'Versailes Elegansa 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '55', 'MDS31802_300x300.jpg', 'Versailes Strisce 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '56', 'glamour_3_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '57', 'glamour_2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '58', 'glamour.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '59', '_______________.jpg', 'Perline Argento 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '60', '______.jpg', 'Listello Glamour 80*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '61', '_______.jpg', 'Alzata Glossy Black 150*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '62', '______.jpg', 'Glamour Bianco rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '63', '______.jpg', 'Boiserie Glossy Black rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '64', 'Vetro_Contrassto_Cristallo.jpg', 'Vetro Contrassto Cristallo 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '65', 'Vetro_Nero_Cristallo.jpg', 'Vetro Nero Cristallo 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '66', 'IMG024.jpg', 'Glossy White 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '67', 'IMG028.jpg', 'Glossy Black 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '68', 'Provence______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '69', '__________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '70', 'TD49002RB__2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '71', 'TR89002J1__800X200.jpg', 'Provence Crema Altaza 200*800', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '73', 'TR89002J2__800X80.jpg', 'Provence Crema Torello 80*800', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '74', '________ORO.jpg', 'Perline Oro 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '75', 'TR89002B__800X400-1.jpg', 'Provence Crema rett. 400*800', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '76', 'TR89006B__400X800-1.jpg', 'Provenсe Cuscino Inserto 400*800', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '77', 'Pietra_Onyx_Via.jpg', 'Pietra Onyx Via 298*298', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '78', 'TD49002RB__400X400-1.jpg', 'Provenсe Crema Tacche 400*400', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '79', 'elysse2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '80', 'elysee.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '81', 'angolo-elysee-crema-25-80.png', 'Angolo Elysee Crema 25х80', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '82', 'spigolo-elysee-crema-32x150.png', 'Spigolo Elysee Crema 32х150', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '83', 'listello-elysee-piselli-80x350.jpg', 'Listello Elysee Piselli 80х350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '84', 'battiscopa-elysee-crema-150x350.jpg', 'Battiscopa Elysee Crema 150х350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '85', 'elysee-boiserie-decoro-rett-350-750.jpg', 'Elysee Boiserie Decoro rett. 350х750', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '86', 'elysee-boiserie-lily-rett-350-750.jpg', 'Elysee Boiserie Lily rett. 350х750', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '87', 'elysee-crema-ornamento-350-350.jpg', 'Elysee Crema Ornamento 350х350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '90', 'Dolce_Vita_Listello_125x300.jpg', 'Dolce Vita Listello 125*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '91', 'Dolce_Vita_London_50x300.jpg', 'Dolce Vita London 50*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '92', '________Turchese.jpg', 'Perline Turchese 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '93', '_________________Dolce_Vita_Lily_rett._300x450.jpg', 'Dolce Vita Lily rett. 300*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '94', '_________________Dolce_Vita_Acqua_rett.300x450.jpg', 'Dolce Vita Acqua rett. 300*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '95', 'MBD30026_300x300.jpg', 'Dolce Vita 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '96', '______Dolce_Vita.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '97', '____________________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '98', '______Dolomite.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '99', '______Dolomite_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '100', '_3.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '101', 'IMG013.jpg', 'Dolomite Ivory rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '102', 'IMG017.jpg', 'Dolomite Mokka rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '103', 'Dolomite_Rombo_Mokka.jpg', 'Dolomite Rombo Mokka 280*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '104', 'Pietra_Marrone_.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '105', 'Pietra_Imperador_Motivo.jpg', 'Pietra Imperador Motivo 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '106', 'IMG076.jpg', 'Dolomite Ivory pol. 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '107', 'IMG075.jpg', 'Dolomite Mokka pol. 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '108', '_____________.jpg', 'Dolomite Rosone Ivory 1200*1200', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '109', '____________.jpg', 'Dolomite Rosone Mokka 1200*1200', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '110', 'Bisell.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '111', 'Bisell_2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '112', 'YS73501-A_120x270.jpg', 'Bisell Listello A 120*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '113', 'YS73501-B_120x270.jpg', 'Bisell Listello B 120*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '114', 'YS73501-C_120x270.jpg', 'Bisell Listello C 120*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '115', 'YJ73501-1_133x800.jpg', 'Fascia Bisell 133*800', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '116', '2FPA73501A_270x730.jpg', 'Bisell Crema rett. 270*730', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '117', '2FPA73501C_270x730.jpg', 'Bisell Boiserie rett. 270*730', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '118', '2FPA73501B_270x730.jpg', 'Bisell Boiserie Fiori rett. 270*730', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '119', 'Creativo_Dolci_Beige_.jpg', 'Creativo Dolci Beige 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '120', '2EPB37501A_300x300.jpg', 'Bisell Crema 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '121', '2EPB37501B_300x300.jpg', 'Bisell Fiori 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '122', '_________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '123', '__________-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '124', '__________________.jpg', 'Listello Louisiana Black 70*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '125', '_________________.jpg', 'Listello Louisiana White 70*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '126', 'Louisiana_Black_rett.jpg', 'Louisiana Black rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '127', 'Louisiana_White_rett.jpg', 'Louisiana White rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '128', '____________________.jpg', 'Glossy White rett. 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '129', 'IMG024.jpg', 'Glossy White 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '130', 'IMG028.jpg', 'Glossy Black 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '131', 'Madagaskar2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '132', 'Madagaskar.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '133', 'MCP37077B1__30x750mm.jpg', 'Madagaskar Listello 30*750', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '134', 'MCP37077__350x750mm.jpg', 'Madagaskar Azzurro rett. 350*750', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '135', 'Argento_Quadro.jpg', 'Argento Quadro 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '136', 'MCD35077__350x350mm__MCD300277__300x300mm.jpg', 'Madagaskar Azzurro 350*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '137', '________2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '138', '________2_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '139', 'Batterfly.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '140', '_____.jpg', 'Butterfly di Fiori inserto s/2 480*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '141', '______.jpg', 'Butterfly Beige rett. 240*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '142', '_______.jpg', 'Vetro Farfalla Rosa 298*298', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '143', '___1.jpg', 'Butterfly Beige tacche 330*330', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '145', 'Nadine_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '146', 'Nadine.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '147', 'YF73201-1_150x270.jpg', 'Alzata Nadine 150*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '148', 'YS73201-A_120x270.jpg', 'Nadine Listello A 120*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '149', 'YS73201-B_120x270.jpg', 'Nadine Listello B 120*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '150', 'YS73201-C_120x270.jpg', 'Nadine Listello C 120*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '151', '1FPA73201A_270X730.jpg', 'Nadine Crema rett. 270*730', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '152', '1FPA73201C_270X730.jpg', 'Nadine Beige rett. 270*730', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '153', '1FPA73201B___270X730.jpg', 'Nadine Boiserie rett. 270*730', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '154', '1EPB37201B__300X300.jpg', 'Nadine Beige 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '155', '1PB37201A.jpg', 'Nadine Crema 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '156', '_15_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '157', '_15.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '158', '_____________________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '159', '_______.jpg', 'La Opera Tozzetto Bordeaux 70*70', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '160', 'IMG080.jpg', 'La Opera Tozzetto Oro 70*70', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '161', 'MEM37827C1.jpg', 'La Opera Bordeaux 23*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '162', 'MEM37827C5__60x350MM.jpg', 'La Opera Capitello 60*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '163', 'MEM37827C6__350x135mm.jpg', 'Battiscopa La Opera 135*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '164', 'MEM37827C11_C2_.jpg', 'La Opera Oro 23*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '165', 'MEM37827G1_185x350mm.jpg', 'La Opera Cassettone185*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '166', 'new-EM37827D5-25x135.png', 'Spigolo La Opera 25*135', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '167', 'new-MEM37827D10.png', 'Angolo Capitello La Opera 20*60', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '168', 'MCS37000__350x750mm.jpg', 'La Opera rett. 350*750', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '169', 'MEM37827__350x750mm.jpg', 'La Opera Boiserie rett. 350*750', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '170', '___.jpg', 'La Opera Esagono 350*350', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '173', 'Primavera_Luche_.jpg', 'Primavera Luche s/3 720*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '174', '________Bronzo.jpg', 'Perline Bronzo 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '175', '________Crema.jpg', 'Perline Crema 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '176', 'Primavera_Luche_rett.jpg', 'Primavera Luche rett. 240*660', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '177', 'Pietra_Onyx_Verde.jpg', 'Pietra Onyx Verde 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '178', 'Primavera_Crema_Tacche.jpg', 'Primavera Crema Tacche 330*330', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '179', 'Primavera_.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '180', 'Primavera__-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '181', '1_Ornamento_Scala_Beige_110_300.jpg', 'Ornamento Scala Beige 110*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '182', '2_Spigolo_Scala_Beige_17_300.jpg', 'Spigolo Scala Beige 17*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '183', '3_Ornamento_Scala_Beige_40_300.jpg', 'Ornamento Scala Beige 40*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '184', '4_Capitel_Scala_Beige_50_300.jpg', 'Capitel Scala Beige 50*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '185', '5_Alzata_Scala_Beige_200_300.jpg', 'Alzata Scala Beige 200*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '186', '______2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '187', '6_Moldura_Scala_Beige__50_300.jpg', 'Moldura Scala Beige 50*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '188', '7_Cenefa_Scala_Beige_120_300.jpg', 'Cenefa Scala Beige 120*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '189', 'scala.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '190', '8_Zocalo_Scala_Beige_200_300.jpg', 'Zocalo Scala Beige 200*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '191', '9_Listello_Scala_Beige_100_300.jpg', 'Listello Scala Beige 100*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '192', '10_Suite_Lines_Scala_Beige_300_902.jpg', 'Suite Lines Scala Beige 300*902', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '193', '11_Scala__Beige_300_600_CCW.jpg', 'Scala Beige 300х600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '194', '12_Scala__Beige_300_902.jpg', 'Scala Beige 300х902', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '195', '13_Scala_Lapato_Beige_430_430.jpg', 'Scala Lapato Beige 430х430', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '196', '18.______1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '197', '16.______3.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '198', '17.______4.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '199', '2.________.jpg', 'Moldura Campania Beige 50*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '200', '3.Campania_______.jpg', 'Spigolo Campania Beige 17*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '201', '4._______12_30.jpg', 'Cenefa Campania Beige 120*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '202', '5._______________.jpg', 'Perline Perla 8*30', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '203', '6.Campania_________.jpg', 'Capitel Campania Beige 50*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '204', '7.Campania________4_30.jpg', 'Ornamento Campania Beige 40*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '205', '8.Campania________10_30.jpg', 'Listello Campania Beige 100*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '206', '9.Campania________11_30.jpg', 'Ornamento Campania Beige 110*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '207', '11._____________.jpg', 'Perline Perla 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '208', '15.________2.jpg', 'Alzata Campania Beige 200*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '209', '10_CCW.jpg', 'Campania Beige 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '210', '13_CCW.jpg', 'Suite Lines Campania Beige 300*902', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '211', '14._______30_90_2.jpg', 'Campania Beige 300*902', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '212', '12.___.jpg', 'Campania Lapato Beige 430*430', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '213', '13.______2.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '214', '1.CFA_CALACATTA_BL_30X12.jpg', 'Сenefa Calacatta Blanco 120*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '215', '2.Calacatta_______.jpg', 'Spigolo Calacatta Blanco 17*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '216', '3.Calacatta________4_30.jpg', 'Ornamento Calacatta Blanco 40*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '217', '4.Calacatta________10_30.jpg', 'Listello Calacatta Blanco 100*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '218', '5.Calacatta________11_30.jpg', 'Ornamento Calacatta Blanco 110*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '219', '6.Calacatta________.jpg', 'Alzata Calacatta Blanco 200*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '220', '7.Calacatta__________1.jpg', 'Moldura Calacatta Blanco 50*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '221', '8.Calacatta_________.jpg', 'Listello Calacatta Blanco 100*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '222', '9.ZOCALO_CALACATTA_BL_30X20.jpg', 'Zocalo Calacatta Blanco 200*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '223', '10.SUITE_LINES_CALACATTA_R_30X90_2.jpg', 'Suite Lines Calacatta 300*902', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '224', '11_CCW.jpg', 'Calacatta Blanco 300*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '225', '14.CALACATTA_BL_R_30X90_2_2_CCW.jpg', 'Calacatta Blanco 300*902', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '226', '12.CALACATTA_LAPATO_BL_43X43_R_2.jpg', 'Calacatta Lapato Blanco 430*430', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '228', '__________Duo_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '229', '__________Duo_2-1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '230', '__________Duo.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '231', 'DECORDADO-DUO-BLANCO_g.jpg', 'Decor Duo Blanco 325*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '232', 'LISTON-DUO-BLANCO_g.jpg', 'Liston Duo Blanco 50*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '233', 'DUO-BLANCO_g.jpg', 'Duo Blanco 325*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '234', 'DUO-MALVA0_g.jpg', 'Duo Malva 325*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '235', 'duo-32_5-malva_g.jpg', 'Duo Malva 01 325*325', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '236', '_____________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '237', 'FANAL-STUDIO.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '238', 'Studio-1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '239', 'DECOR_STUDIO_IVORY_A.jpg', 'Decor Studio Ivory A 250*500', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '240', 'DECOR_STUDIO_IVORY_B.jpg', 'Decor Studio Ivory B 250*500', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '241', '5281STUDIO-NATURAL-RELIEVE_g.jpg', 'Studio Sand Reliev 250*500', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '242', 'STUDIO-IVORY_rec_g.jpg', 'Studio Ivory 250*500', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '243', 'STUDIO-IVORY-RELIEVE_g.jpg', 'Studio Ivory Reliev 250*500', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '244', 'STUDIO-SAND_G.jpg', 'Studio Sand 250*500', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '245', 'STUDIO_IVORY_1.jpg', 'Mosaico Studio Ivory A 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '246', 'STUDIO-IVORY-45_g.jpg', 'Studio Ivory 450*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '247', 'STUDIO-SAND-45X45_g.jpg', 'Studio Sand 450*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '248', '01f_AMB_IBERO_CREMA_DECOR_MOSAICO_GOLD.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '249', '16c_amb_IBERO_BEIGE_NATURAL_ROSETON_TARRACO.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '250', '013d_RACO._TARRACO_IBERO.preview2.jpg', 'Taco Rarraco 22x22', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '251', '12b_CENEFA_MERIDA_NATURAL.preview2.jpg', 'Cenefa Merida Gold Natural 14.2x31.6', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '252', '01a_CENEFA_MERIDA_BEIGE.preview2.jpg', 'Cenefa Merida Gold Beige 14.2x31.6', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '253', '10b_zocalo_ibero_natural.preview2.jpg', 'Zocalo Ibero Natural 16x31.6', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '254', '06a_zocalo_ibero_beige.preview2.jpg', 'Zocalo Ibero Beige 16x31.6', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '255', '09b_Moldura_MERIDA_natural.preview2.jpg', 'Capitel Merida Natural 5x31.6', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '256', '02a_diff_Moldura_MERIDA_beige.preview2.jpg', 'Capitel Merida Gold Beige 5x31.6', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '257', '03a_IBERO_BEIGE.jpg', 'Ibero Beige 31,6x60', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '258', '05IBERO_natural_VOLUTAS_ORO.jpg', 'Décor Ibero Natural 31.6x60', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '259', '08b_IBERO_NATURAL.jpg', 'Ibero Natural 31,6x60', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '260', '15IBERO_beige_VOLUTAS_ORO.jpg', 'Décor Ibero Beige 31.6x60', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '261', '04a_IBERO_BEIGE_.jpg', 'Ibero Beige 45x45', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '262', '07b_IBERO_NATURAL_.jpg', 'Ibero Natural 45x45', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '264', '14c_ALMERA_IBERO_ROSETON_TARRACO_45X45.preview2.jpg', 'Roseton Tarraco 45x45', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '265', '011c_CNF._TARRACO_IBERO.preview2.jpg', 'Cenefa Tarraco 22x45', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '266', '____________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '267', '_____________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '268', 'Palace_Beige_Rosone_1200x1200.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '269', 'Matita.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '270', 'Argenta_Branelli_Matita.jpg', 'Argenta Branelli Matita 27*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '271', 'Oro_Branelli_Matita.jpg', 'Oro Branelli Matita 27*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '272', '_______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '273', '______5.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '274', '______6.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '275', '_______________.jpg', 'Perline Argento 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '276', '_____________.jpg', 'Perline Perla 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '277', '________Bronzo.jpg', 'Perline Bronzo 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '278', '________Crema.jpg', 'Perline Crema 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '279', '________ORO.jpg', 'Perline Oro 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '280', '________Turchese.jpg', 'Perline Turchese 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '281', 'Serie_Di_Creativo.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '282', 'Creativo_Bronzo_Fisica.jpg', 'Creativo Bronzo Fisica 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '283', 'Creativo_Crema_Nevicata.jpg', 'Creativo Crema Nevicata 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '284', 'Creativo_Dolci_Beige_.jpg', 'Creativo Dolci Beige 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '285', 'Creativo_Fiore_di_Neve_.jpg', 'Creativo Fiore di Neve 304*304', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '286', 'Creativo_Fondali_.jpg', 'Creativo Fondali 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '287', 'Creativo_Mare_Azzurro.jpg', 'Creativo Mare Azzurro 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '288', 'Creativo_Pearl.jpg', 'Creativo Pearl 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '289', 'Creativo_Rosa_Nevicata_.jpg', 'Creativo Rosa Nevicata 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '290', 'Creativo_Trend.jpg', 'Creativo Trend 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '291', 'Creativo_Viola_Nevicata.jpg', 'Creativo Viola Nevicata 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '292', 'Serie_Di_Oro.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '293', 'Oro_Di_Mare.jpg', 'Oro di Mare 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '294', 'Oro_Di_Montana.jpg', 'Oro di Montana 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '295', 'Oro_Lamina.jpg', 'Oro Lamina 310*310', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '296', 'Oro_Nebbia.jpg', 'Oro Nebbia 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '297', 'Oro_Nuvola.jpg', 'Oro Nuvola 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '298', 'Oro_Quadro.jpg', 'Oro Quadro 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '299', 'Serie_Di_Argento-1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '300', 'Argento_Di_Montana.jpg', 'Argento Di Montana 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '301', 'Argento_Quadro.jpg', 'Argento Quadro 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '302', 'Serie_di_Vetro.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '303', '_______.jpg', 'Vetro Farfalla Rosa 298*298', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '304', 'Vetro_Contrassto_Cristallo.jpg', 'Vetro Contrassto Cristallo 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '305', 'Vetro_Latte_Bijou.jpg', 'Vetro Latte Bijou 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '306', 'Vetro_Marmellata_Blue.jpg', 'Vetro Marmellata Blue 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '307', 'Vetro_Marmellata_Vino.jpg', 'Vetro Marmellata Vino 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '308', 'Vetro_Nero_Cristallo.jpg', 'Vetro Nero Cristallo 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '309', 'Vetro_Shocolate_Bijou.jpg', 'Vetro Shocolate Bijou 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '310', 'Vetro_Sole_Cristallo.jpg', 'Vetro Sole Cristallo 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '311', 'Vetro_Stella_Cristallo.jpg', 'Vetro Stella Cristallo 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '312', 'Serie_Di_Petra.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '313', 'Pietra_Calcare_Oro_.jpg', 'Pietra Calcare Oro 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '314', 'Pietra_Gotica_Piccola.jpg', 'Pietra Gotica Piccola 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '315', 'Pietra_Gotica.jpg', 'Pietra Gotica 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '316', 'Pietra_Imperador_Motivo.jpg', 'Pietra Imperador Motivo 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '317', 'Pietra_Marrone_.jpg', 'Pietra Marrone 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '318', 'Pietra_Onyx_Ampia_298x298.jpg', 'Pietra Onyx Ampia 298*298', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '319', 'Pietra_Onyx_Calza.jpg', 'Pietra Onyx Calza 280*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '320', 'Pietra_Onyx_Grande.jpg', 'Pietra Onyx Grande 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '321', 'Pietra_Onyx_Mollica.jpg', 'Pietra Onyx Mollica 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '322', 'Pietra_Onyx_Verde.jpg', 'Pietra Onyx Verde 305*305', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '323', 'Pietra_Onyx_Via.jpg', 'Pietra Onyx Via 298*298', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '324', '_________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '325', 'Storie_Di_Pietra_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '326', 'Storie_Di_Pietra.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '327', 'Pietra_Gotica.jpg', 'Pietra Gotica 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '328', 'Storie_Di_Pietra_Palazzo_Bargello.jpg', 'Storie Di Pietra Palazzo Bargello 270*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '329', 'Storie_Di_Pietra_Palazzo_Pitti_.jpg', 'Storie Di Pietra Palazzo Pitti 270*270', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '330', '_________3_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '331', '_________3.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '332', 'Acero.jpg', 'Lamella Sbiancato 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '333', 'Faggio.jpg', 'Lamella Faggio 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '334', 'Merbau.jpg', 'Lamella Merbau 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '335', 'Sbiancato.jpg', 'Lamella Acero 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '336', 'Wenge.jpg', 'Lamella Wenge 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '337', 'Oro_Marmi_2_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '338', 'ORO_Marmi.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '339', 'Oro_Marmi_Tozzetto_145x145.jpg', 'Oro Marmi Tozzetto 145*145', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '340', 'Oro_Marrmi_Listello_150x600.jpg', 'Oro Marrmi Listello 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '341', 'Oro_Marmi_600x600.jpg', 'Oro Marmi 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '342', '_______________-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '343', '______________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '344', 'Listello_Royale_150x300.jpg', 'Listello Royale 150*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '345', 'Tozzetto_Royale_150x150.jpg', 'Tozzetto Royale 150*150', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '346', 'Pietra_Onyx_Ampia_298x298.jpg', 'Pietra Onyx Ampia 298*298', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '347', 'Royale_A_300x300.jpg', 'Royale A 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '348', 'Royale_B_300x300.jpg', 'Royale B 300*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '349', '___________-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '350', '__________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '351', 'Palace_Beige_Fascia_150x600.jpg', 'Palace Beige Fascia 150*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '352', 'Palace_Beige_Tozzetto_150x150.jpg', 'Palace Beige Tozzetto 150*150', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '353', 'Palace_Beige_600x600_.jpg', 'Palace Beige 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '355', 'Palace_Beige_Rosone_1200x1200.jpg', 'Palace Beige Rosone 1200*1200', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '356', '______1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '357', '______4.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '358', 'Della_Pelle_______-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '359', 'della_pelle1-1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '360', '____________1.jpg', 'Della Pelle Panna Decoro B 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '361', '____________2.jpg', 'Della Pelle Panna Decoro A 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '362', '__________11.jpg', 'Della Pelle Melanzana Decoro B 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '363', '__________22.jpg', 'Della Pelle Melanzana Decoro A 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '364', '___________________.jpg', 'Della Pelle Listello Quadro Panna 20*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '365', '__________________.jpg', 'Della Pelle Listello Quadro Melanzana 20*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '366', '_______________.jpg', 'Della Pelle Listello Panna 20*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '367', '______________.jpg', 'Della Pelle Listello Melanzana 20*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '368', 'Della_Pelle_Tozzetto_Panna_20x20.jpg', 'Della Pelle Tozzetto Melanzana 20*20', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '369', 'Tozzetto_Melanzana_20x20.jpg', 'Della Pelle Tozzetto Panna 20*20', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '370', 'Della_Pelle_Melanzana.JPG', 'Della Pelle Melanzana 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '371', 'Della_Pelle_Panna_.JPG', 'Della Pelle Panna 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '372', 'Country.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '373', 'Country2_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '374', 'Country2-1.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '375', 'Carelia_150x900.jpg', 'Carelia 150*900', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '376', 'Danimarca_150x900.jpg', 'Danimarca 150*900', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '377', 'Finlandia_150x900.jpg', 'Finlandia 150*900', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '378', 'Islanda_150x900.jpg', 'Islanda 150*900', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '379', 'Norvegia_150x900.jpg', 'Norvegia 150*900', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '380', 'Marrakesh_-______.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '381', 'Marrakesh.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '382', '______.jpg', 'Marrakesh 600*600', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '383', '__________________.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '384', 'Nocturne.jpg', NULL, NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '385', 'Nocturne_Fiore_Inserto_300x450.jpg', 'Nocturne Fiore Inserto 300*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '386', '_____________.jpg', 'Perline Perla 8*300', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '387', 'Nocturne_Confetto_Listello_150x450.jpg', 'Nocturne Confetto Listello 150*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '388', 'Nocturne_Spigolo_Righe_20x450.jpg', 'Nocturne Spigolo Righe 20*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '389', 'Nocturne_Confetto_rett.jpg', 'Nocturne Confetto rett. 300*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '390', 'Nocturne_Rosa_rett._300x450.jpg', 'Nocturne Rosa rett. 300*450', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '391', 'Creativo_Fiore_di_Neve_.jpg', 'Creativo Fiore di Neve 304*304', NULL, NULL );
INSERT INTO `photos`(`id`,`photo`,`name`,`body`,`root`) VALUES ( '392', 'Nocturne_Confett.jpg', 'Nocturne Confetto 300*300', NULL, NULL );
-- ---------------------------------------------------------


-- Dump data of "photos_products" --------------------------
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '24', '7' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '38', '8' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '40', '9' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '48', '10' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '49', '10' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '56', '11' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '57', '11' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '58', '11' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '68', '12' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '69', '12' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '70', '12' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '79', '13' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '80', '13' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '96', '14' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '97', '14' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '98', '15' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '99', '15' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '100', '15' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '110', '16' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '111', '16' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '122', '17' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '123', '17' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '131', '18' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '132', '18' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '137', '19' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '138', '19' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '139', '19' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '145', '20' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '146', '20' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '156', '21' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '157', '21' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '158', '21' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '179', '22' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '180', '22' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '186', '24' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '189', '24' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '196', '25' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '197', '25' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '198', '25' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '213', '26' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '228', '27' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '229', '27' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '230', '27' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '236', '28' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '237', '28' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '238', '28' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '248', '29' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '249', '29' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '266', '30' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '267', '31' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '268', '32' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '269', '33' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '272', '34' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '273', '34' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '274', '34' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '281', '35' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '292', '36' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '299', '37' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '302', '38' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '312', '39' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '324', '40' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '325', '40' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '326', '40' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '330', '41' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '331', '41' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '337', '42' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '338', '42' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '342', '43' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '343', '43' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '349', '44' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '350', '44' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '356', '45' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '357', '45' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '358', '45' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '359', '45' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '372', '46' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '373', '46' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '374', '46' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '380', '47' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '381', '47' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '383', '48' );
INSERT INTO `photos_products`(`photo_id`,`product_id`) VALUES ( '384', '48' );
-- ---------------------------------------------------------


-- Dump data of "photos_sub_products" ----------------------
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '26', '1' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '27', '1' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '28', '1' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '29', '1' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '30', '1' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '31', '1' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '32', '2' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '33', '2' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '34', '2' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '35', '2' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '36', '3' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '37', '3' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '39', '4' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '41', '5' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '42', '6' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '43', '6' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '44', '6' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '45', '7' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '46', '8' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '47', '9' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '50', '10' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '51', '10' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '52', '11' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '53', '12' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '54', '12' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '55', '12' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '59', '13' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '60', '13' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '61', '13' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '62', '14' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '63', '14' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '64', '15' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '65', '15' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '66', '16' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '67', '16' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '71', '17' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '73', '17' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '74', '17' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '75', '18' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '76', '18' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '77', '19' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '78', '20' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '81', '21' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '82', '21' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '83', '21' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '84', '21' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '85', '22' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '86', '22' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '87', '23' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '90', '24' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '91', '24' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '92', '24' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '93', '25' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '94', '25' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '95', '26' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '101', '27' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '102', '27' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '103', '28' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '104', '28' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '105', '28' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '106', '29' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '107', '29' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '108', '30' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '109', '30' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '112', '31' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '113', '31' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '114', '31' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '115', '31' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '116', '32' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '117', '32' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '118', '32' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '119', '33' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '120', '34' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '121', '34' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '124', '35' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '125', '35' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '126', '36' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '127', '36' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '128', '36' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '129', '37' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '130', '37' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '133', '38' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '134', '39' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '135', '40' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '136', '41' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '140', '42' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '141', '43' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '142', '44' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '143', '45' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '147', '46' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '148', '46' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '149', '46' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '150', '46' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '151', '47' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '152', '47' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '153', '47' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '154', '48' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '155', '48' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '159', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '160', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '161', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '162', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '163', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '164', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '165', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '166', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '167', '49' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '168', '50' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '169', '50' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '170', '51' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '173', '52' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '174', '53' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '175', '53' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '176', '54' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '177', '55' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '178', '56' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '181', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '182', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '183', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '184', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '185', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '187', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '188', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '190', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '191', '58' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '192', '59' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '193', '59' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '194', '59' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '195', '60' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '199', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '200', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '201', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '202', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '203', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '204', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '205', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '206', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '207', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '208', '61' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '209', '62' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '210', '62' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '211', '62' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '212', '63' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '214', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '215', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '216', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '217', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '218', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '219', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '220', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '221', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '222', '64' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '223', '65' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '224', '65' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '225', '65' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '226', '66' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '231', '67' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '232', '68' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '233', '69' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '234', '69' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '235', '70' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '239', '71' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '240', '71' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '241', '72' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '242', '72' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '243', '72' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '244', '72' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '245', '73' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '246', '74' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '247', '74' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '250', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '251', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '252', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '253', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '254', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '255', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '256', '75' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '257', '76' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '258', '76' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '259', '76' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '260', '76' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '261', '77' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '262', '77' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '264', '77' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '265', '77' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '270', '81' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '271', '81' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '275', '82' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '276', '82' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '277', '82' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '278', '82' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '279', '82' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '280', '82' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '282', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '283', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '284', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '285', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '286', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '287', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '288', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '289', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '290', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '291', '83' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '293', '84' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '294', '84' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '295', '84' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '296', '84' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '297', '84' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '298', '84' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '300', '85' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '301', '85' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '303', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '304', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '305', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '306', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '307', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '308', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '309', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '310', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '311', '86' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '313', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '314', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '315', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '316', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '317', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '318', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '319', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '320', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '321', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '322', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '323', '87' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '327', '88' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '328', '89' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '329', '89' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '332', '90' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '333', '90' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '334', '90' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '335', '90' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '336', '90' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '339', '91' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '340', '91' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '341', '92' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '344', '93' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '345', '93' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '346', '94' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '347', '95' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '348', '95' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '351', '96' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '352', '96' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '353', '97' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '355', '98' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '360', '99' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '361', '99' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '362', '99' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '363', '99' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '364', '100' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '365', '100' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '366', '100' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '367', '100' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '368', '100' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '369', '100' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '370', '101' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '371', '101' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '375', '102' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '376', '102' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '377', '102' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '378', '102' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '379', '102' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '382', '103' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '385', '104' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '386', '105' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '387', '105' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '388', '105' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '389', '106' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '390', '106' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '391', '107' );
INSERT INTO `photos_sub_products`(`photo_id`,`sub_product_id`) VALUES ( '392', '108' );
-- ---------------------------------------------------------


-- Dump data of "products" ---------------------------------
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '7', 'Miracolo', NULL, '0', NULL, NULL, '2014-10-22 12:43:09', '2014-10-22 12:57:57', '1' );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '8', 'Коллекция Voyage 30*60', 'Компания "ДМ-Стайл" стремится шагать в ногу со временем и старается регулярно пополнять и обновлять свои коллекции керамической плитки.

Одной из самых лучших наших находок стало раскрытие морской тематики в керамической коллекции "Vojage". Настенная плитка в светло-бежевых тонах с фоновым рисунком, имитирующим прибрежный песок, выгодно подчеркнет изысканность вашей ванной комнаты.А декоративные вставки с изображением ракушек, морских звезд и гальки придадут помещению завершенный вид.
Керамогранит глазурованный с ректификационной кромкой. Выпускается в размерах: стена 300х600, пол 300х300, декоры ( два вида рисунка ) 300х600.', '0', NULL, NULL, '2014-10-22 13:03:29', '2014-10-22 13:04:33', '0' );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '9', 'Коллекция Pastel 240x660', 'Коллекция керамической плитки Pastel от VILLA CERAMICA создана для ванных комнат. Настенная плитка имитирует мрамор c нежно - розоватым оттенком . В состав коллекции входит сборное цветочное панно из 4-х элементов, которое можно выкладывать как снизу так и сверху стены, тем самым зонируя помещение. Также коллекция доукомплектована эффектной мозаикой из камня и стекла и узкими бордюрами со стеклянными капельками -«бусинками» кремового и бронзового цветов.', '0', NULL, NULL, '2014-10-22 13:07:08', '2014-10-22 13:10:43', '1' );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '10', 'Коллекция Versailes 300*300', NULL, '0', NULL, NULL, '2014-10-22 16:32:10', '2014-10-22 16:33:02', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '11', 'Коллекция Glamour 300*600', 'Коллекция керамической плитки Glamour   от VILLA CERAMICA создана для ванных комнат. Чёрно-белая, монохромная гамма коллекции в совокупности со стеклянной мозаикой тех же оттенков создаёт  действительно потрясающий эффект. Напольная плитка дополнена вставками с имитацией бриллиантов, которые создают изумительную игру света  в отражении  освещения.  Чёрная плитка выполнена в виде фасадов в стиле «буазере», а светлая плитка декорирована пиктограммами под Louis Vuitton.', '0', NULL, NULL, '2014-10-22 16:37:59', '2014-10-22 16:41:30', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '12', 'Коллекция Provence 400*800', 'Коллекция керамической плитки Provence от VILLA CERAMICA имитирует поверхность среза благородного кремово-го мрамора и выполнена в духе минимализма. Особую изюминку коллекции придают элементы в виде панели из фальш-ромбов, которая отпускается кратно квадратным метрам. Размер керамической плитки 400*800, но несмотря на свой немаленький размер, с ней очень удобно работать при укладке, а ректифицированная кромка обеспечит монолитную укладку. В итоге, даже стандартных размеров, ванная комната, облицованная плиткой из коллекции Provence, будет выглядеть богато, стильно и современно.', '0', NULL, NULL, '2014-10-22 16:43:13', '2014-10-22 16:46:24', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '13', 'Коллекция Elysse 350*750', 'Плитка из коллекции "ELYSSE" предназначена для оформления дорогих и изысканных помещений. Она идеально подойдёт как для ванных комнат так и для гостинных и прихожих. Серия керамической плитки "ELYSSE" от «Villa Ceramica» придется по вкусу тем,  кто  любит эксклюзивные и запоминающиеся интерьеры, вызывающие всеобщее восхищение. Классическая обойная тематика декорации в коллекции "ELYSSE" прекрасно впишется интерьер любого помещения.', '0', NULL, NULL, '2014-10-22 16:50:20', '2014-10-23 11:31:18', '1' );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '14', 'Коллекция Dolce Vita 300*450', 'Коллекция «Dolce Vita», разработанная итальянскими дизайнерами, выполнена в модной и актуальной стилистике. Изящный орнамент в стиле "барокко" с геральдическими лилиями для создания непрерывного обойного узора создаст неповторимую атмосферу в помещении. Материал отличается удивительным дизайном поверхности, который актуален для данного жанрового направления, а также представлен в изысканном и фантастически красивом серо-голубом цвете.', '0', NULL, NULL, '2014-10-22 16:54:11', '2014-10-22 16:58:09', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '15', 'Коллекция Dolomite 300*600', 'Настенная и напольная плитка, а также ромбовидная мозаика произведена из полированного керамогранита. В коллекции Dolomite от VILLA CERAMICA присутствуют контрастные оттенки кремового и тёмно- бежевого цветов. Элементы коллекции отлично воспроизводят поверхность натурального мрамора, что делает коллекцию идеальным облицовочным покрытиями для санузлов, гостиных и входных зон. Коллекция также доукомплектована двумя видами мозаики -  стеклянной и с имитацией камня, с помощью которых можно создать неповторимые настенные бордюры, и в итоге, получить эксклюзивный интерьер.', '0', NULL, NULL, '2014-10-22 17:04:16', '2014-10-22 17:08:17', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '16', 'Коллекция Bisell 270*730', 'Коллекция керамической плитки "BISELL"  от VILLA CERAMICA станет стильным и изысканным украшением для фешенебельной ванной комнаты. Основными чертами коллекции являются нежность, изящество и благородство. Коллекция выполнена в сочетании классики и романтики. Пастельные тона, необычная фактура, нежный цветочный рисунок на декорах стен и полов — всё это свидетельствует о гармоничности и совершенстве коллекции керамической плитки "BISELL".', '0', NULL, NULL, '2014-10-22 17:10:01', '2014-10-22 17:13:28', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '17', 'Коллекция Louisiana 300*600', 'Плитка под кожу крокодила – это не просто оригинальный дизайн в вашем интерьере, это еще и отличный качественный отделочный материал. Очень лаконично и сдержанно смотрится сочетание плитки светлых и темных тонов. Современные технологии дают возможность воспроизводить различные фактуры на разного рода поверхностях. Правда, стоит отметить, что такая плитка стоит несколько дороже, чем обычная керамическая плитка, но не стоит скупиться на роскошь. «Кожаная» плитка – сильный акцент, напоминание о первобытных инстинктах, элемент экзотики и одновременно свидетельство респектабельности и состоятельности хозяев.', '0', NULL, NULL, '2014-10-22 17:15:24', '2014-10-22 17:18:07', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '18', 'Коллекция Madagaskar 300*600', 'Коллекция «Madagaskar» может быть использована для создания минималистической ванной или роскошного дизайнерского проекта. Особую изюминку добавляет стеклянный узкий бордюр с эффектом застывшей лавы.
Размер облицовочной плитки в коллекции 350*750 мм, что даже в небольшом помещении позволяет эффектно выложить плитку как в горизонтальном так и вертикальном положении.
Облицовочная плитка произведена на белой глине и ректифицирована , что при укладке позволяет добиться максимального эстетического эффекта.', '0', NULL, NULL, '2014-10-22 17:19:27', '2014-10-22 17:21:44', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '19', 'Коллекция Butterfly 240*660', 'Коллекция керамической плитки Butterfly от VILLA CERAMICA превратит вашу ванную комнату или кухню, в сад с бабочками. Нежные пастельные тона керамической плитки в совокупности с изумительным панно, с изображением играющих бабочек, не оставят никого равнодушным. Коллекция дополнена стеклянной мозаикой, в которой воедино собраны как глянцевые, так и матовые элементы. Практичным хозяевам придётся по душе напольная плитка из коллекции Butterfly , произведённая с тактильной поверхностью, исключающей скольжение.', '0', NULL, NULL, '2014-10-22 17:23:20', '2014-10-22 17:25:43', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '20', 'Коллекция Nadine 270*730', 'Новая коллекция керамической плитки «Nadine» от компании «Villa Ceramica» можно будет приобрести уже в апреле 2013 года, которая прекрасно подойдет для создания роскошных помещений: это может быть как ванная комната и кухня, так и спальня и гостиная. Продукция отличается очень привлекательным внешним видом и оригинальным дополнением в виде декоративных элементов «буазере». Используя данные отделочные материалы можно создать эксклюзивный интерьер, который будет вызывать восхищённые взгляды. Ручная работа и передовые технологии дают возможность сделать коллекцию керамической плитки «Nadine» действительно уникальной. Она универсальна, а потому ее можно применять для отделки ванных комнат спален, кухонь и гостиных - такая плитка будет создавать в любом помещении атмосферу уюта и гармонии.', '0', NULL, NULL, '2014-10-22 17:26:15', '2014-10-22 17:30:15', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '21', 'Коллекция La Opera 350*750', 'Богатая, можно сказать богемная, коллекция "La Opera" выполнена в благородном цвете слоновой кости с использованием декоративных элементов в двух вариантах: бордово-красном и золотом. Коллекция имеет глянцевую глушёную поверхность плитки. Особую привлекательность коллекции придают декоративные элементы выполненные в популярном сегодня среди производителей керамической плитки стиле «буазере» - имитация декоративных узорных панелей на стене. Формат настенной плитки из коллекции—350*750. Такая плитка будет выигрышно смотреться как в объёмных, так и в небольших помещениях. Коллекцию "La Opera" одинаково успешно можно использовать как в санузлах, так и на кухне или в гостиной.', '0', NULL, NULL, '2014-10-22 17:32:33', '2014-10-22 17:36:50', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '22', 'Коллекция Primavera 240x660', 'Коллекция керамической плитки Primavera от VILLA CERAMICA выполнена в нежном оттенке бежевого с изумительным панно в виде гербария из кленовых листьев. Коллекция идеально подойдёт для облицовки ванных комнат и кухонь. Напольная плитка выполнена с тактильным эффектом, что очень важно при использовании в помещениях доступом к воде, так как вероятность подскользнуться на такой плитке сведена к минимуму. Коллекция доукомплектована очень красивой мозаикой из камня с бирюзовыми стеклянными вкраплениями.', '0', NULL, NULL, '2014-10-22 17:37:55', '2014-10-22 17:40:37', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '24', 'Scala', NULL, '0', NULL, NULL, '2014-10-22 17:46:12', '2014-10-22 17:50:39', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '25', 'Campania', NULL, '0', NULL, NULL, '2014-10-22 18:11:21', '2014-10-22 18:15:52', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '26', 'Коллекция Calacatta', 'Коллекция Calacatta выполнена в роскошной имитации мраморного покрытия, искусно расцвеченного насыщенными, стильными оттенками различных цветов. Роскошная плитка теплыми переливами вольется в каждый частный интерьер или офисное помещение. Изысканные оттенки плитки Calacatta завораживают неподражаемой красотой и дизайнерским исполнением. Прожилки серого цвета лаконично рассыпаны на однотонном основании керамических изделий. Плитка для стен и пола является идеальным сочетанием, как между собой, так и с другими облицовочными материалами, независимо от их фактуры. Коллекция Сalacatta – плитка отменного качества. Производство плитки ведется на высококлассном, инновационном оборудовании, поэтому товар полностью соответствует европейским стандартам облицовочных материалов.', '0', NULL, NULL, '2014-10-22 18:18:24', '2014-10-22 18:22:27', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '27', 'Коллекция DUO 325*600', 'Эта красивая и элегантная коллекция керамической плитки DUO от испанской фабрики FANAL CERAMICA обязательно понравится большинству покупателей. Крупноформатная плитка, удобного для укладки формата 325*600, сочных и нежных лиловых оттенков в совокупности с цветочными бордюрами и декорами преобразят ванную комнату и сделают её романтичной и гармоничной. А фактурная особенность плитки воссоздана и на декоративных элементах: бордюрах и декорах. Коллекция в новом для себя лиловом цвете была впервые представлена на выставке Cevisama 2013 в Валенсии и произвела там настоящий фурор среди посетителей.', '0', NULL, NULL, '2014-10-22 18:40:33', '2014-10-22 18:43:13', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '28', 'Коллекция STUDIO 250*500', NULL, '0', NULL, NULL, '2014-10-22 18:44:49', '2014-10-22 18:49:06', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '29', 'Ibero', NULL, '0', NULL, NULL, '2014-10-22 18:53:48', '2014-10-22 18:59:24', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '30', 'Dolomite Rosone Mokka 1200*1200', NULL, '0', NULL, NULL, '2014-10-22 19:03:08', '2014-10-22 19:03:41', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '31', 'Dolomite Rosone Ivory 1200*1200', NULL, '0', NULL, NULL, '2014-10-22 19:04:29', '2014-10-22 19:05:03', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '32', 'Напольное панно Palace Beige Rosone 1200*1200', NULL, '0', NULL, NULL, '2014-10-22 19:05:30', '2014-10-22 19:05:51', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '33', 'Карандаши', NULL, '0', NULL, NULL, '2014-10-22 19:07:00', '2014-10-22 19:08:02', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '34', 'Бусинки', 'Универсальные бордюры - "бусинки"  подойдут практически к любым коллекциям керамической плитки Villa Ceramica. Размер "бусинок" - 300*8 мм что позоляет их эффектно использовать для того, чтобы разделять светлую и тёмную зоны коллекций плитки, обрамлять бордюры и декоры, выделять на контрасте фоновую плитку. Многие при выборе декоративных элементов для коллекции ограничиваются только бусинками разных цветов и в итоге делают свой интерьер уникальным и неповторимым.', '0', NULL, NULL, '2014-10-22 19:09:41', '2014-10-22 19:11:49', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '35', 'Serie Di Creativo', NULL, '0', NULL, NULL, '2014-10-22 19:15:11', '2014-10-22 19:17:58', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '36', 'Serie Di Oro', 'Мозаика с имитацией золота становится на российском рынке популярней с каждым днём. Согласитесь, что мало кто откажется добавить в свой интерьер кусочек гламура. Мозаика из серии Serie Di Oro от Villa Ceramica прекрасно впишется в любой интерьер и создаст ощущение благородства и изысканности. Также рекомендуем дополнить интерьер с использованием мозаики из серии Serie Di Oro сантехникой и аксекссуарами с позолоченными элементами для большей гармоничности.', '0', NULL, NULL, '2014-10-22 19:19:32', '2014-10-22 19:21:42', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '37', 'Мозаика Serie Di Argento', 'SERIE DI ARGENTO - Серия мозаики, включающая в себя элементы имитирующие «серебрянную» поверхность. Идеально подходит для облицовки и выделения зон в помещении. Идеально дополняет коллекции VILLA CERAMICA выполненные в холодных тонах.', '0', NULL, NULL, '2014-10-22 19:22:45', '2014-10-22 19:24:01', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '38', 'Мозаика Serie Di Vetro', 'Стеклянная мозаика - очень популярный на сегодняшний день материал. Эта мозаика производится из стандартных компонентов (полевой шпат, кварцевый песок и сода с оксидами металла) путём их переплавки,штамповки и последующим охлаждением. Так уж сложилось что практически 90% производства стеклянной мозаики, включая известнейшие мировые бренды, ксконцентрировано в Китае. 

Стеклянная мозаика достаточно прочная, но и в свое время она очень хрупкая. В связи с этим её не рекомендуется укладывать на пол. Сегодня существует достаточно интересный тренд - использование стеклянной мозаики как элемент декора. Кусочками и обрезками мозаики декорируют плитку, обрамляют бордюры, выделяют зоны. Тем самым создаются очень самобытные интерьеры.', '0', NULL, NULL, '2014-10-22 19:25:12', '2014-10-22 19:27:51', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '39', 'Мозаика Serie Di Petra', 'Мозаика из камня - очень интересный материал для отделки стен и полов. В ассортименте DM-Style представлен большой ассортимент мозаики из этого материала. Есть мозаика с имитацией "оникса", "доломита", "мрамора". Есть мозаика из камня с вкраплением стеклянных, металлических и керамических вставок, с имитацией золота и серебра.  Мозаика из камня является также прекрасным декоративным элементом. Не случайно она входит в состав многих коллекций "Villa Ceramica".', '0', NULL, NULL, '2014-10-22 19:29:12', '2014-10-22 19:32:20', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '40', 'Storie Di Pietra', 'Представляем вашему вниманию коллекцию глазурованного керамогранита Storie Di Pietra от Villa Ceramica. Коллекция специально разработана для использования вне помещения и прекрасно подойдёт для облицовки полов и стен на террасах, зимних садах, верандах и входных групп. Напольная плитка производится в виде восьмиугольников (октагонов) 270*270,  которые соединяются между собой с помощью вставок  50*50. Коллекция доукомплектована мозаикой также выполненной  с имитацией камня, что позволяет её гармонично вписать в интерьер помещения.', '0', NULL, NULL, '2014-10-22 19:34:37', '2014-10-22 19:36:16', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '41', 'Lamella 150*600', 'Керамогранитные плитки из коллекции "LAMELLA" представляют собой имитацию деревянных напольных покрытий, поверхность которых повторяет рисунок природного материала и при этом имеет лёгкую структурированность.  Элементы выполнены из керамогранита с ректифицированной кромкой, что позволяет производить практически бесшовную укладку. Керамогранит "LAMELLA" можно применять для облицовки полов и стен как снаружи так и внутри помещения.', '0', NULL, NULL, '2014-10-23 09:49:02', '2014-10-23 11:25:08', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '42', 'Oro Marmi 600*600', 'Плитка из коллекции Oro Marmi очень достоверно имитирует мрамор серии Emperiador Dark. Это точное воспроизведение натурального мрамора с одноименным названием, который является одним из самых популярных видов мрамора в мире. Керамогранит под мрамор данного типа имеет насыщенный темно-коричневый или каштановый цвет, с густой сетью прожилок светлых оттенков, которые составляют замысловатый рисунок.', '0', NULL, NULL, '2014-10-23 11:09:55', '2014-10-23 11:11:38', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '43', 'Royale 300*300', 'Коллекция "ROYALE" от VILLA CERAMICA имитирует такой популярнейший материал как камень «оникс». Плитка отличается универсальностью—её можно выкладывать как на пол, так и на стены. Поверхность покрыта дымчатой «гашёной» глазурью и имеет два типа сборных узора, которые в ковре создают благородные и изысканные интерьеры. Коллекция «ROYALE» от "VILLA Ceramica" - это сочетание функциональности и красоты.', '0', NULL, NULL, '2014-10-23 11:11:45', '2014-10-23 11:14:37', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '44', 'Palace 600*600', 'Коллекция "PALACE" прекрасно имитирует натуральный камень в формате 600*600 мм, а также отличается зеркальным блеском и бежево-золотистым сиянием. Основная линия продукции дополняется красивыми бордюрами, вставками и панно - все это дает возможность создавать действительно уникальные и стильные композиции, которые будут вызывать восхищенные взгляды.', '0', NULL, NULL, '2014-10-23 11:14:39', '2014-10-23 11:16:48', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '45', 'Della Pelle 600*600', 'Коллекция Della Pelle от VILLA Ceramica имитирует высококачественную выделанную кожу кремового и коричневого оттенков. Отделочный материал можно использовать как для отделки ванных комнат и кухонь, так и для оформления гостиных и спален. Довольно интересно он будет смотреться в прихожей. Особую изюминку коллекции придают декоративные элементы:
два вида бордюров ( со швейной стёжкой и с узором в стиле «Gucci»), а также великолепные сборные декоры с узорами,также выполненные с имитацией вышивки. Причём они отпускаются не по-штучно, а кратно квадратным метрам, что делает роскошь доступнее.', '0', NULL, NULL, '2014-10-23 11:16:57', '2014-10-23 11:21:39', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '46', 'Country 150*900', 'Коллекция производится из ректифицированного керамогранита 150*900 и выполнена в разнообразных оттенках бежевого цвета, а также оттенке дуба. Плитка из коллекции «COUNTRY» имеет достаточно стильный дизайн, отличается такими качествами как функциональность, практичность, износостойкость и долговечность. Используя данные отделочные материалы можно создать уютное и красивое помещение, которое будет доставлять радость. Коллекция рекомендуется для использования в общественных местах (магазины, рестораны, торговые центры), именно поэтому каждый покупатель, сможет заметить, что она невероятно функциональна, износостойка, прочна, надежна и практична.', '0', NULL, NULL, '2014-10-23 11:21:41', '2014-10-23 11:23:44', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '47', 'Marrakesh', 'Коллекция напольного глазурованного ректифицированного керамогранита "Marrakesh" выполнена в формате 600*600 мм. Дизайн плитки выполнен в богато оформленной восточной стилистике, с изумительно красивым позолоченным обрамлением кромки. Поверхность плитки –глазурь 4-го обжига, которая показывает всю глубину и красоту мрамора, срез которого имитирует дизайн плитки.', '0', NULL, NULL, '2014-10-23 11:23:46', '2014-10-23 11:24:48', NULL );
INSERT INTO `products`(`id`,`name`,`body`,`price`,`sale`,`art`,`created_at`,`updated_at`,`new`) VALUES ( '48', 'Nocturne 300*450', 'Коллекция Nocturne от VILLA CERAMICA выполнена в нежно розовых цветах и прекрасно подойдёт для оформления санузлов. Колkекция доукомплектована стеклянной мозаикой с цветочным орнаментом и кремовыми бусинками, которые заметно оживляют коллекцию. Небольшой размер настенной плитки - 300*450 идеально подойдёт для стандартных саyузлов. Также хочется обратить внимание на декоры в виде мозаичного панно, из которых собирается изображение роз.', '0', NULL, NULL, '2014-10-23 11:38:21', '2014-10-23 11:42:28', '0' );
-- ---------------------------------------------------------


-- Dump data of "products_properties" ----------------------
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '7', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '7', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '7', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '8', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '8', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '8', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '9', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '9', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '9', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '10', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '10', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '10', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '11', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '11', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '11', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '12', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '12', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '12', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '13', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '13', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '13', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '14', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '14', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '14', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '15', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '15', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '15', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '16', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '16', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '16', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '17', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '17', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '17', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '18', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '18', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '18', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '19', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '19', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '19', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '20', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '20', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '20', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '21', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '21', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '21', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '22', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '22', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '22', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '24', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '25', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '25', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '25', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '26', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '26', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '26', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '27', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '27', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '27', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '28', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '28', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '28', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '29', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '29', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '29', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '33', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '33', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '33', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '34', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '34', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '34', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '35', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '35', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '35', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '36', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '36', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '36', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '37', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '37', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '37', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '38', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '38', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '38', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '39', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '39', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '39', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '40', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '40', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '40', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '41', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '41', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '41', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '42', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '42', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '42', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '43', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '43', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '43', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '44', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '44', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '44', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '45', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '45', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '45', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '46', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '46', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '46', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '47', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '47', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '47', '3' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '48', '1' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '48', '2' );
INSERT INTO `products_properties`(`product_id`,`property_id`) VALUES ( '48', '3' );
-- ---------------------------------------------------------


-- Dump data of "products_values" --------------------------
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '7', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '7', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '7', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '8', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '8', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '8', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '9', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '9', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '9', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '10', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '10', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '10', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '11', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '11', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '11', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '12', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '12', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '12', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '13', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '13', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '13', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '14', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '14', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '14', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '15', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '15', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '15', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '16', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '16', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '16', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '17', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '17', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '17', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '18', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '18', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '18', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '19', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '19', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '19', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '20', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '20', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '20', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '21', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '21', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '21', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '22', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '22', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '22', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '24', '6' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '25', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '25', '6' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '25', '7' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '26', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '26', '6' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '26', '7' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '27', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '27', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '27', '6' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '28', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '28', '4' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '28', '6' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '29', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '29', '6' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '29', '7' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '33', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '33', '8' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '33', '9' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '34', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '34', '8' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '34', '9' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '35', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '35', '10' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '35', '11' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '36', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '36', '11' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '36', '12' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '37', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '37', '11' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '37', '13' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '38', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '38', '11' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '38', '14' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '39', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '39', '11' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '39', '15' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '40', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '40', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '40', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '41', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '41', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '41', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '42', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '42', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '42', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '43', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '43', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '43', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '44', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '44', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '44', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '45', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '45', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '45', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '46', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '46', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '46', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '47', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '47', '5' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '47', '16' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '48', '2' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '48', '3' );
INSERT INTO `products_values`(`product_id`,`value_id`) VALUES ( '48', '4' );
-- ---------------------------------------------------------


-- Dump data of "properties" -------------------------------
INSERT INTO `properties`(`id`,`name`,`style`,`unit`,`sortable`) VALUES ( '1', 'Производитель', NULL, NULL, NULL );
INSERT INTO `properties`(`id`,`name`,`style`,`unit`,`sortable`) VALUES ( '2', 'Назначение', NULL, NULL, NULL );
INSERT INTO `properties`(`id`,`name`,`style`,`unit`,`sortable`) VALUES ( '3', 'Материал', NULL, NULL, NULL );
-- ---------------------------------------------------------


-- Dump data of "rich_rich_files" --------------------------
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '1', '2014-10-21 07:18:29', '2014-10-21 07:18:29', 'miracolo.jpg', 'image/jpeg', '885227', '2014-10-21 07:18:27', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/001/thumb/miracolo.jpg","large":"/system/rich/rich_files/rich_files/000/000/001/large/miracolo.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/001/rich_thumb/miracolo.jpg","original":"/system/rich/rich_files/rich_files/000/000/001/original/miracolo.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '2', '2014-10-21 07:18:58', '2014-10-21 07:18:58', 'hmlc60362a.jpg', 'image/jpeg', '244358', '2014-10-21 07:18:57', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/002/thumb/hmlc60362a.jpg","large":"/system/rich/rich_files/rich_files/000/000/002/large/hmlc60362a.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/002/rich_thumb/hmlc60362a.jpg","original":"/system/rich/rich_files/rich_files/000/000/002/original/hmlc60362a.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '3', '2014-10-21 07:19:06', '2014-10-21 07:19:06', 'hmlb60361a-300x600-.jpg', 'image/jpeg', '36432', '2014-10-21 07:19:06', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/003/thumb/hmlb60361a-300x600-.jpg","large":"/system/rich/rich_files/rich_files/000/000/003/large/hmlb60361a-300x600-.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/003/rich_thumb/hmlb60361a-300x600-.jpg","original":"/system/rich/rich_files/rich_files/000/000/003/original/hmlb60361a-300x600-.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '4', '2014-10-21 07:22:02', '2014-10-21 07:22:02', 'storie-20di-20pietra.jpg', 'image/jpeg', '560780', '2014-10-21 07:22:01', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/004/thumb/storie-20di-20pietra.jpg","large":"/system/rich/rich_files/rich_files/000/000/004/large/storie-20di-20pietra.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/004/rich_thumb/storie-20di-20pietra.jpg","original":"/system/rich/rich_files/rich_files/000/000/004/original/storie-20di-20pietra.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '5', '2014-10-23 11:28:23', '2014-10-23 11:28:23', 'marrakesh.jpg', 'image/jpeg', '167250', '2014-10-23 11:28:22', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/005/thumb/marrakesh.jpg","large":"/system/rich/rich_files/rich_files/000/000/005/large/marrakesh.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/005/rich_thumb/marrakesh.jpg","original":"/system/rich/rich_files/rich_files/000/000/005/original/marrakesh.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '6', '2014-10-23 11:28:46', '2014-10-23 11:28:46', 'della-20pelle-20-d1-8d-d1-81-d0-ba-d0-b8-d0-b7-20-20-d0-ba-d0-be-d0-bf-d0-b8-d1-8f.jpg', 'image/jpeg', '52193', '2014-10-23 11:28:46', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/006/thumb/della-20pelle-20-d1-8d-d1-81-d0-ba-d0-b8-d0-b7-20-20-d0-ba-d0-be-d0-bf-d0-b8-d1-8f.jpg","large":"/system/rich/rich_files/rich_files/000/000/006/large/della-20pelle-20-d1-8d-d1-81-d0-ba-d0-b8-d0-b7-20-20-d0-ba-d0-be-d0-bf-d0-b8-d1-8f.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/006/rich_thumb/della-20pelle-20-d1-8d-d1-81-d0-ba-d0-b8-d0-b7-20-20-d0-ba-d0-be-d0-bf-d0-b8-d1-8f.jpg","original":"/system/rich/rich_files/rich_files/000/000/006/original/della-20pelle-20-d1-8d-d1-81-d0-ba-d0-b8-d0-b7-20-20-d0-ba-d0-be-d0-bf-d0-b8-d1-8f.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '7', '2014-10-23 11:29:13', '2014-10-23 11:29:13', 'pastel.jpg', 'image/jpeg', '89518', '2014-10-23 11:29:13', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/007/thumb/pastel.jpg","large":"/system/rich/rich_files/rich_files/000/000/007/large/pastel.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/007/rich_thumb/pastel.jpg","original":"/system/rich/rich_files/rich_files/000/000/007/original/pastel.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '8', '2014-10-27 05:39:42', '2014-10-27 05:39:42', 'sclad.jpg', 'image/jpeg', '143898', '2014-10-27 05:39:41', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/008/thumb/sclad.jpg","large":"/system/rich/rich_files/rich_files/000/000/008/large/sclad.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/008/rich_thumb/sclad.jpg","original":"/system/rich/rich_files/rich_files/000/000/008/original/sclad.jpg"}', 'image' );
INSERT INTO `rich_rich_files`(`id`,`created_at`,`updated_at`,`rich_file_file_name`,`rich_file_content_type`,`rich_file_file_size`,`rich_file_updated_at`,`owner_type`,`owner_id`,`uri_cache`,`simplified_type`) VALUES ( '9', '2014-10-27 05:39:47', '2014-10-27 05:39:47', 'sclad1.jpg', 'image/jpeg', '52355', '2014-10-27 05:39:47', NULL, NULL, '{"thumb":"/system/rich/rich_files/rich_files/000/000/009/thumb/sclad1.jpg","large":"/system/rich/rich_files/rich_files/000/000/009/large/sclad1.jpg","rich_thumb":"/system/rich/rich_files/rich_files/000/000/009/rich_thumb/sclad1.jpg","original":"/system/rich/rich_files/rich_files/000/000/009/original/sclad1.jpg"}', 'image' );
-- ---------------------------------------------------------


-- Dump data of "schema_migrations" ------------------------
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140829213217' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140830140747' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140830215018' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140830220650' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140901175701' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140904200155' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140904200156' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140921062115' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140924173641' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140924174231' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140927110329' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140928071620' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141006172002' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141006172133' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141006174331' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141011073656' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141011104636' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141011104637' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141011104638' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141016172135' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141016172207' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141016172246' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141018061009' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141018061842' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141019125404' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141020045201' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20141020045202' );
-- ---------------------------------------------------------


-- Dump data of "sub_products" -----------------------------
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '1', 'Декоративные элементы', NULL, '7' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '2', 'Настенная плитка', NULL, '7' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '3', 'Плитка для пола', NULL, '7' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '4', 'Декоративные элементы', NULL, '8' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '5', 'Декоры', NULL, '9' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '6', 'Декоративные элементы', NULL, '9' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '7', 'Настенная плитка', NULL, '9' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '8', 'Мозаика', NULL, '9' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '9', 'Плитка для пола', NULL, '9' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '10', 'Декоративные элементы', NULL, '10' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '11', 'Мозаика', NULL, '10' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '12', 'Плитка для пола', NULL, '10' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '13', 'Декоративные элементы', NULL, '11' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '14', 'Настенная плитка', NULL, '11' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '15', 'Мозаика', NULL, '11' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '16', 'Плитка для пола', NULL, '11' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '17', 'Декоративные элементы', NULL, '12' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '18', 'Настенная плитка', NULL, '12' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '19', 'Мозаика', NULL, '12' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '20', 'Плитка для пола', NULL, '12' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '21', 'Декоративные элементы', NULL, '13' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '22', 'Настенная плитка', NULL, '13' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '23', 'Плитка для пола', NULL, '13' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '24', 'Декоративные элементы', NULL, '14' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '25', 'Настенная плитка', NULL, '14' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '26', 'Плитка для пола', NULL, '14' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '27', 'Настенная плитка', NULL, '15' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '28', 'Мозаика', NULL, '15' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '29', 'Плитка для пола', NULL, '15' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '30', 'Панно из керамогранита', NULL, '15' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '31', 'Декоративные элементы', NULL, '16' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '32', 'Настенная плитка', NULL, '16' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '33', 'Мозаика', NULL, '16' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '34', 'Плитка для пола', NULL, '16' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '35', 'Декоративные элементы', NULL, '17' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '36', 'Настенная плитка', NULL, '17' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '37', 'Плитка для пола', NULL, '17' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '38', 'Декоративные элементы', NULL, '18' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '39', 'Настенная плитка', NULL, '18' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '40', 'Мозаика', NULL, '18' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '41', 'Плитка для пола', NULL, '18' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '42', 'Декоративные элементы', NULL, '19' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '43', 'Настенная плитка', NULL, '19' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '44', 'Мозаика', NULL, '19' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '45', 'Плитка для пола', NULL, '19' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '46', 'Декоративные элементы', NULL, '20' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '47', 'Настенная плитка', NULL, '20' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '48', 'Плитка для пола', NULL, '20' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '49', 'Декоративные элементы', NULL, '21' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '50', 'Настенная плитка', NULL, '21' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '51', 'Плитка для пола', NULL, '21' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '52', 'Декоры', NULL, '22' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '53', 'Декоративные элементы', NULL, '22' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '54', 'Настенная плитка', NULL, '22' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '55', 'Мозаика', NULL, '22' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '56', 'Плитка для пола', NULL, '22' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '57', 'Декоративные элементы', NULL, '23' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '58', 'Декоративные элементы', NULL, '24' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '59', 'Настенная плитка', NULL, '24' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '60', 'Плитка для пола', NULL, '24' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '61', 'Декоративные элементы', NULL, '25' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '62', 'Настенная плитка', NULL, '25' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '63', 'Плитка для пола', NULL, '25' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '64', 'Декоративные элементы', NULL, '26' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '65', 'Настенная плитка', NULL, '26' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '66', 'Плитка для пола', NULL, '26' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '67', 'Декоры', NULL, '27' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '68', 'Декоративные элементы', NULL, '27' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '69', 'Настенная плитка', NULL, '27' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '70', 'Плитка для пола', NULL, '27' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '71', 'Декоры', NULL, '28' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '72', 'Настенная плитка', NULL, '28' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '73', 'Мозаика', NULL, '28' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '74', 'Плитка для пола', NULL, '28' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '75', 'Декоративные элементы', NULL, '29' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '76', 'Настенная плитка', NULL, '29' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '77', 'Плитка для пола', NULL, '29' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '81', 'Декоративные элементы', NULL, '33' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '82', 'Декоративные элементы', NULL, '34' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '83', 'Мозаика', NULL, '35' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '84', 'Мозаика', NULL, '36' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '85', 'Мозаика', NULL, '37' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '86', 'Мозаика', NULL, '38' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '87', 'Мозаика', NULL, '39' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '88', 'Мозаика', NULL, '40' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '89', 'Плитка для пола', NULL, '40' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '90', 'Плитка для пола', NULL, '41' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '91', 'Декоративные элементы', NULL, '42' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '92', 'Плитка для пола', NULL, '42' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '93', 'Декоративные элементы', NULL, '43' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '94', 'Мозаика', NULL, '43' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '95', 'Плитка для пола', NULL, '43' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '96', 'Декоративные элементы', NULL, '44' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '97', 'Плитка для пола', NULL, '44' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '98', 'Панно из керамогранита', NULL, '44' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '99', 'Декоры', NULL, '45' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '100', 'Декоративные элементы', NULL, '45' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '101', 'Плитка для пола', NULL, '45' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '102', 'Плитка для пола', NULL, '46' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '103', 'Плитка для пола', NULL, '47' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '104', 'Декоры', NULL, '48' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '105', 'Декоративные элементы', NULL, '48' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '106', 'Настенная плитка', NULL, '48' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '107', 'Мозаика', NULL, '48' );
INSERT INTO `sub_products`(`id`,`name`,`body`,`product_id`) VALUES ( '108', 'Плитка для пола', NULL, '48' );
-- ---------------------------------------------------------


-- Dump data of "values" -----------------------------------
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '2', '1', 'Италия-Китай' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '3', '2', 'Плитка для ванной' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '4', '3', 'Белая глина' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '5', '3', 'Глазурованный керамогранит' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '6', '1', 'Испания' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '7', '3', 'Керамическая плитка' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '8', '3', 'Керамика-стекло' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '9', '2', 'Декоративные элементы' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '10', '3', 'Мозаика-"микс" из стекла, керамики и камня' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '11', '2', 'Мозаика' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '12', '3', 'Имитация золота' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '13', '3', 'имитация поверхности из серебра' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '14', '3', 'Стекло' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '15', '3', 'Камень' );
INSERT INTO `values`(`id`,`property_id`,`value`) VALUES ( '16', '2', 'Напольная плитка' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


